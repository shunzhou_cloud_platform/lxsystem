import axios from 'axios'

const request = (url,body,type = 'GET') => {
    return new Promise((resolve,reject) => {
        axios({
            method: type,
            url,
            data:body
        }).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err)
        })
    })
}
export default request;