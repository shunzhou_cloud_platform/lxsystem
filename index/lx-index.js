//良信中控页面js
var tab_0,tab_1,tab_2,tab_3,wan_tab,WAN_s_info,senior_dhcp_tab,senior_tab,senior_dhcp_static_tab,high_grade_function_firewall_interval,high_grade_function_firewall_interval_mb,request = true;
$(function(){
	index_data();
	$(".tab_item").on("click",function(){
		var tab_index = $(this).index();	
		if(tab_index == 1){
			if(tab_0!=undefined){clearInterval(tab_0);}
			if(WAN_s_info!=undefined){clearInterval(WAN_s_info);}
			if(wan_tab!=undefined){clearInterval(wan_tab);}				
			if(tab_3!=undefined){clearInterval(tab_3);}
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			tab_1 = setInterval(function(){
				XHR.get('<%=luci.dispatcher.build_url("admin", "status", "info")%>',{status:1},function(x, info){
					var Aptables = info;
					console.log(Aptables);
				});	
			}, 5000);	
		}
		if(tab_index == 2){
			if(tab_0!=undefined){clearInterval(tab_0);}
			if(tab_1!=undefined){clearInterval(tab_1);}
			if(wan_tab!=undefined){clearInterval(wan_tab);}				
			if(tab_3!=undefined){clearInterval(tab_3);}
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			//get WAN State
			WANState_info();
		}
		if(tab_index == 3){
			if(tab_0!=undefined){clearInterval(tab_0);}
			if(tab_1!=undefined){clearInterval(tab_1);}
			if(WAN_s_info!=undefined){clearInterval(WAN_s_info);}
			if(wan_tab!=undefined){clearInterval(wan_tab);}		
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			if(tab_3!=undefined){clearInterval(tab_3);}
			
			network_ctrl_set();
			network_ctrl_table();
		}
		if(tab_index == 4){				
			if(tab_0!=undefined){clearInterval(tab_0);}
			if(tab_1!=undefined){clearInterval(tab_1);}
			if(WAN_s_info!=undefined){clearInterval(WAN_s_info);}
			if(wan_tab!=undefined){clearInterval(wan_tab);}				
			
			high_grade_function_4gconfig();
		}
		if(tab_index == 5){				
			if(tab_0!=undefined){clearInterval(tab_0);}
			if(tab_1!=undefined){clearInterval(tab_1);}
			if(WAN_s_info!=undefined){clearInterval(WAN_s_info);}
			if(wan_tab!=undefined){clearInterval(wan_tab);}				
			if(tab_3!=undefined){clearInterval(tab_3);}
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			set_password_init();
		}
	});
	$('.WANsave').on('click',function () {
		var protocolVal = $(".WANprotocol").val();
		var regIP = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
		var _sendName = $(".sendName input").val();
		var _IPInput = 	$('.IPInput input').val();
		var _smask =	$('.smask input').val();
		var _networkgate =	$('.networkgate input').val();
		var _borad = $('.borad input').val();
		var _DNS = $('.DNS input').val();
		var _PPOEUser =$('.PPOEUser input').val();
		var _PPOEPWD = $('.PPOEPWD input').val();
		if(protocolVal == 'dhcp'){
			if(_sendName=='') {
        		layer.open({
            		title: '提示信息'
            		,content: '输入内容不能为空,请重新输入'
            	});
	        }else{
    			WANprotocol("dhcp");
	        	event.preventDefault();
			}
	    }else if(protocolVal == 'static'){
			if((_IPInput=='')||(_smask=='')||(_networkgate=='')||(_borad=='')||(_DNS=='')){
        		layer.open({
            		title: '提示信息'
            		,content: '输入内容不能为空,请重新输入'
            	});
	        }else{
				if(!regIP.test(_smask)||!regIP.test(_IPInput)||!regIP.test(_networkgate)||!regIP.test(_borad)||!regIP.test(_DNS)){
					layer.open({
            			title: '提示信息'
            			,content: '输入格式不正确，请重新输入'
            		});	
				}else{
					WANprotocol("static");
		    	    event.preventDefault();	
				}				
			}
    	}else if(protocolVal == 'pppoe'){
			if((_PPOEUser=='')||(_PPOEPWD=='')){
        		layer.open({
            		title: '提示信息'
                	,content: '输入内容不能为空,请重新输入'
	        	});
    	    }else{
       			WANprotocol("PPPOE");
		        event.preventDefault();				
			}	
	    }		
    });
	$(".tab3_sub_senior").on("click",function(){
		var tab3_sub_index = $(this).index();
		if(tab3_sub_index == 0){
			console.log("4G网络配置");
			if(senior_tab!=undefined){clearInterval(senior_tab);};
			if(senior_dhcp_static_tab!=undefined){clearInterval(senior_dhcp_static_tab);};
			if(senior_dhcp_tab!=undefined){clearInterval(senior_dhcp_tab);};
			if(high_grade_function_firewall_interval!=undefined){clearInterval(high_grade_function_firewall_interval);};
			
			high_grade_function_4gconfig();
		}
		if(tab3_sub_index == 1){
			console.log("DHCP");
			if(senior_tab!=undefined){clearInterval(senior_tab);};
			if(high_grade_function_firewall_interval!=undefined){clearInterval(high_grade_function_firewall_interval);};

			senior_DHCP(); 
			senior_DHCP_newadd();
			senior_DHCP_static();
			DHCPSaveApply();
		}
		if(tab3_sub_index == 2){
			console.log("防火墙");
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			if(senior_dhcp_static_tab!=undefined){clearInterval(senior_dhcp_static_tab);};
			if(senior_dhcp_tab!=undefined){clearInterval(senior_dhcp_tab);};

			high_grade_function_firewall();
		}
		if(tab3_sub_index == 3){
			console.log("网络诊断");
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			if(senior_dhcp_static_tab!=undefined){clearInterval(senior_dhcp_static_tab);};
			if(senior_dhcp_tab!=undefined){clearInterval(senior_dhcp_tab);};
			if(high_grade_function_firewall_interval!=undefined){clearInterval(high_grade_function_firewall_interval);};

			high_grade_function_network_init();
		}
		if(tab3_sub_index == 4){
			console.log("RS485");
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			if(senior_dhcp_static_tab!=undefined){clearInterval(senior_dhcp_static_tab);};
			if(senior_dhcp_tab!=undefined){clearInterval(senior_dhcp_tab);};
			if(high_grade_function_firewall_interval!=undefined){clearInterval(high_grade_function_firewall_interval);};

			high_grade_function_rs();
		}
		if(tab3_sub_index == 5){
			console.log("日志");
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			if(senior_dhcp_static_tab!=undefined){clearInterval(senior_dhcp_static_tab);};
			if(senior_dhcp_tab!=undefined){clearInterval(senior_dhcp_tab);};
			if(high_grade_function_firewall_interval!=undefined){clearInterval(high_grade_function_firewall_interval);};

			high_grade_function_log();
		}
		if(tab3_sub_index == 6){
			console.log("Zigbee参数");
			if(senior_dhcp_static_tab!=undefined){clearInterval(senior_dhcp_static_tab);};
			if(senior_dhcp_tab!=undefined){clearInterval(senior_dhcp_tab);};
			if(high_grade_function_firewall_interval!=undefined){clearInterval(high_grade_function_firewall_interval);};

			high_grade_function_zigbee();
			senior_tab = setInterval(high_grade_function_zigbee,5000);
		}
	});
	$(".tab3_sub_mb_senior").on("click",function(){
		var tab3_sub_mb_index = $(this).index();
		if(tab3_sub_mb_index == 0){
			console.log("4G网络配置");
			if(senior_tab!=undefined){clearInterval(senior_tab);};
			if(senior_dhcp_static_tab!=undefined){clearInterval(senior_dhcp_static_tab);};
			if(senior_dhcp_tab!=undefined){clearInterval(senior_dhcp_tab);};
			if(high_grade_function_firewall_interval_mb!=undefined){clearInterval(high_grade_function_firewall_interval_mb);};

			high_grade_function_4gconfig();
		}
		if(tab3_sub_mb_index == 1){
			console.log("DHCP");
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			if(high_grade_function_firewall_interval_mb!=undefined){clearInterval(high_grade_function_firewall_interval_mb);};

			senior_DHCP(); 
			senior_DHCP_newadd();
			senior_DHCP_static();
			DHCPSaveApply();
		}
		if(tab3_sub_mb_index == 2){
			console.log("防火墙");
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			if(senior_dhcp_static_tab!=undefined){clearInterval(senior_dhcp_static_tab);};
			if(senior_dhcp_tab!=undefined){clearInterval(senior_dhcp_tab);};

			high_grade_function_firewall_mb();
		}
		if(tab3_sub_mb_index == 3){
			console.log("网络诊断");
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			if(senior_dhcp_static_tab!=undefined){clearInterval(senior_dhcp_static_tab);};
			if(senior_dhcp_tab!=undefined){clearInterval(senior_dhcp_tab);};
			if(high_grade_function_firewall_interval_mb!=undefined){clearInterval(high_grade_function_firewall_interval_mb);};

			high_grade_function_network_init();
		}
		if(tab3_sub_mb_index == 4){
			console.log("RS485");
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			if(senior_dhcp_static_tab!=undefined){clearInterval(senior_dhcp_static_tab);};
			if(senior_dhcp_tab!=undefined){clearInterval(senior_dhcp_tab);};
			if(high_grade_function_firewall_interval_mb!=undefined){clearInterval(high_grade_function_firewall_interval_mb);};

			high_grade_function_rs();
		}
		if(tab3_sub_mb_index == 5){
			console.log("日志");
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			if(senior_dhcp_static_tab!=undefined){clearInterval(senior_dhcp_static_tab);};
			if(senior_dhcp_tab!=undefined){clearInterval(senior_dhcp_tab);};
			if(high_grade_function_firewall_interval_mb!=undefined){clearInterval(high_grade_function_firewall_interval_mb);};
			
			high_grade_function_log();
		}
		if(tab3_sub_mb_index == 6){
			console.log("Zigbee参数");
			if(senior_tab!=undefined){clearInterval(senior_tab);}
			if(senior_dhcp_static_tab!=undefined){clearInterval(senior_dhcp_static_tab);};
			if(senior_dhcp_tab!=undefined){clearInterval(senior_dhcp_tab);};
			if(high_grade_function_firewall_interval_mb!=undefined){clearInterval(high_grade_function_firewall_interval_mb);};

			high_grade_function_zigbee();
			senior_tab = setInterval(high_grade_function_zigbee,5000);
		}
	});
	$(".tab3_sub_sys").on("click",function(){
		var tab3_sub_sys_index = $(this).index();
		if(tab3_sub_sys_index == 0){
			console.log("密码设置");
			set_password_init();
		}
		if(tab3_sub_sys_index == 1){
			console.log("恢复出厂设置");
			restore_init();
		}
		if(tab3_sub_sys_index == 2){
			console.log("备份升级");
			upgrade_init();
		}
		if(tab3_sub_sys_index == 3){
			console.log("周期任务");
			cycle_tasks_init();
		}
		if(tab3_sub_sys_index == 4){
			console.log("重启");
			restart_init();
			$(".reStart").css("display","inline-block");
			$(".reStartWord").css("display","none");
		}
	});
	$(".tab3_sub_mb_sys").on("click",function(){
		var tab3_sub_mb_sys_index = $(this).index();
		if(tab3_sub_mb_sys_index == 0){
			console.log("密码设置");
			set_password_init();
		}
		if(tab3_sub_mb_sys_index == 1){
			console.log("恢复出厂设置");
			restore_init();
		}
		if(tab3_sub_mb_sys_index == 2){
			console.log("备份升级");
			upgrade_init();
		}
		if(tab3_sub_mb_sys_index == 3){
			console.log("周期任务");
			cycle_tasks_init();
		}
		if(tab3_sub_mb_sys_index == 4){
			console.log("重启");
			restart_init();
			$(".reStart").css("display","inline-block");
			$(".reStartWord").css("display","none");
		}
	});
    var choiceFiles = $('.choiceFiles');
    $('.choiceFiles').on('click',function(){
        $('.update').trigger('click');
    });
    var updateLocal = $('.updateLocal');
    $('.updateLocal').on('click',function(){
        $('.upLocal').trigger('click');
		var updateLoal = $("#updateLoal");
		updateLoal.attr('enctype','multipart/form-data');
		updateLoal.attr('method','post');  
		updateLoal.attr('action','<%=luci.dispatcher.build_url("admin", "system", "flashops")%>');
    });
	$(".quit").on("click",function(){
		localStorage.clear();
    	window.location.href = '<%=luci.dispatcher.build_url("admin", "logout")%>';
    });
	var user = localStorage.user;
    $(".userName").html('&nbsp' + user);
});
/** 函数封装 **/	
/**
 * 1. index State
 * */
function index_data(){
	XHR.get('<%=luci.dispatcher.build_url("admin", "status", "info")%>',{status:1},function(x, info){
		//WAN State
		var Aptables = info;
		var wan_time = secondToDate(Aptables.wan.uptime);
		$(".connetTime").html(wan_time);
		if(Aptables.wan.uptime > 0){$(".connetState").html("已连接");}else{$(".connetState").html("未连接");}					
		$(".connetIp").html(Aptables.wan.ipaddr);					
		$(".procolType").html(Aptables.wan.proto);
		$(".macAddr").html(Aptables.wan.macaddr);							
		//SYS State
		var firVer = $(".firmwareVersion").html(Aptables.fver)
		var sys_time = secondToDate(Aptables.uptime);
		$(".runTime").html(sys_time);
		$(".IEEEAddr").html(Aptables.center.ieee);
		$(".RAMRate").html((100-(Aptables.memory.free + Aptables.memory.buffered) * 100 / Aptables.memory.total).toFixed(2) + "%");
		//AP table 
		var Aplists = info.apTable;
		var Aplist_doms_1 = $(".index_list_1");
		var Aplist_doms_2 = $(".index_list_2");
		
		var Aplist_doms_mb =$(".APs_mb_list");
		
		if((Aplists.length < 1)||(JSON.stringify(Aplists) == "{}")){
			$('.APSearch').remove();
			$('.APs_mb_Search').remove();
			var Aplist_doms_1_html ='<div class="flex_h align_center list_item_uniform APSearch"><p>暂无AP连接</p><p>-</p><p>-</p></div>';
			var Aplist_doms_2_html ='<div class="flex_h align_center list_item_uniform APSearch"><p>-</p><p>-</p><p>-</p></div>';
			Aplist_doms_1.append(Aplist_doms_1_html);
			Aplist_doms_2.append(Aplist_doms_2_html);
			
			var Aplist_doms_html_mb = '<div class="list"><div class="list_item flex_h align_center justify_between"><span>序号</span> <span>暂无</span></div> <div class="list_item flex_h align_center justify_between"><span>MAC地址</span> <span>-</span></div> <div class="list_item flex_h align_center justify_between"><span>IP地址</span> <span>-</span></div> <div class="list_item flex_h align_center justify_between"><span>在线人数</span> <span>-</span></div> <div class="list_item flex_h align_center justify_between no-b"><span>状态</span> <span>-</span></div> <div class="list_item flex_h align_center justify_between no-b"><span>版本</span> <span>-</span></div></div>';
			Aplist_doms_mb.append(Aplist_doms_html_mb);
        }else{
			$('.APSearch').remove();
			$('.APs_mb_Search').remove();
			for(var t = 0;t < Aplists.length; t++){
				var linking = Aplists[t].status == 1 ? '已连接' : '未连接';
				var Aplist_doms_1_html ='<div class="flex_h align_center list_item_uniform APSearch"><p>'
										+ (t+1) 
										+'</p><p>'
										+ Aplists[t].macaddr 
										+'</p><p>'
										+ Aplists[t].ip 
										+'</p></div>';
				var Aplist_doms_2_html ='<div class="flex_h align_center list_item_uniform APSearch"><p>'
										+ Aplists[t].ol_num 
										+'</p><p>'
										+ linking 
										+'</p><p>'
										+ Aplists[t].version 
										+'</p></div>';
            	Aplist_doms_1.append(Aplist_doms_1_html);
				Aplist_doms_2.append(Aplist_doms_2_html);
				var Aplist_doms_html_mb = '<div class="list"><div class="list_item flex_h align_center justify_between"><span>序号</span> <span>'
										+ (t+1)
										+'</span></div> <div class="list_item flex_h align_center justify_between"><span>MAC地址</span> <span>'
										+ Aplists[t].macaddr
										+'</span></div> <div class="list_item flex_h align_center justify_between"><span>IP地址</span> <span>'
										+ Aplists[t].ip 
										+'</span></div> <div class="list_item flex_h align_center justify_between"><span>在线人数</span> <span>'
										+ Aplists[t].ol_num
										+'</span></div> <div class="list_item flex_h align_center justify_between no-b"><span>状态</span> <span>'
										+ linking
										+'</span></div> <div class="list_item flex_h align_center justify_between no-b"><span>版本</span> <span>'
										+ Aplists[t].version
										+'</span></div></div>';
				Aplist_doms_mb.append(Aplist_doms_html_mb);
			}
        }
	});	
	tab_0 = setInterval(function(){ 
		XHR.get('<%=luci.dispatcher.build_url("admin", "status", "info")%>',{status:1},function(x, info){
			//WAN State
			var Aptables = info;
			var wan_time = secondToDate(Aptables.wan.uptime);
			$(".connetTime").html(wan_time);
			if(Aptables.wan.uptime > 0){$(".connetState").html("已连接");}else{$(".connetState").html("未连接");}					
			$(".connetIp").html(Aptables.wan.ipaddr);											
			$(".procolType").html(Aptables.wan.proto);
			$(".macAddr").html(Aptables.wan.macaddr);
			//SYS State
			var firVer = $(".firmwareVersion").html(Aptables.fver)
			var sys_time = secondToDate(Aptables.uptime);
			$(".runTime").html(sys_time);
			$(".IEEEAddr").html(Aptables.center.ieee);
			$(".RAMRate").html((100-(Aptables.memory.free + Aptables.memory.buffered) * 100 / Aptables.memory.total).toFixed(2) + "%");
			//AP table 
			var Aplists = info.apTable;
			var Aplist_doms_1 = $(".index_list_1");
			var Aplist_doms_2 = $(".index_list_2");
			if((Aplists.length < 1)||(JSON.stringify(Aplists) == "{}")){
				$('.APSearch').remove();
				var Aplist_doms_1_html ='<div class="flex_h align_center list_item_uniform APSearch"><p>暂无AP连接</p><p>-</p><p>-</p></div>';
				var Aplist_doms_2_html ='<div class="flex_h align_center list_item_uniform APSearch"><p>-</p><p>-</p><p>-</p></div>';
				Aplist_doms_1.append(Aplist_doms_1_html);
				Aplist_doms_2.append(Aplist_doms_2_html);
	        }else{
				$('.APSearch').remove();
				for(var t = 0;t < Aplists.length; t++){
					var linking = Aplists[t].status == 1 ? '已连接' : '未连接'
					Aplist_doms_1_html ='<div class="flex_h align_center list_item_uniform APSearch">'
										+'<p>'+ (t+1) +'</p>'
										+'<p>'+ Aplists[t].macaddr +'</p>'
										+'<p>'+ Aplists[t].ip +'</p></div>';
					Aplist_doms_2_html ='<div class="flex_h align_center list_item_uniform APSearch">'
										+'<p>'+ Aplists[t].ol_num +'</p>'
										+'<p>'+ linking +'</p>'
										+'<p>'+ Aplists[t].version +'</p></div>';
	            	Aplist_doms_1.append(Aplist_doms_1_html);
					Aplist_doms_2.append(Aplist_doms_2_html);
				}
	        }
			
		});	
	}, 5000);	
}
/**
 * 2. WAN State
 * */
function WANState_info(){
	WAN_init();
	WAN_s_info = setInterval(WAN_init, 5000);
	function WAN_init(){
		XHR.get('<%=luci.dispatcher.build_url("admin", "status", "info")%>',{status:2},function(x, info){
			//WAN State
			var Aptables = info;
			var wan_time = secondToDate(Aptables.wan.uptime);
			$("#wanConectTime").html(wan_time);
			if(Aptables.wan.uptime > 0){$("#wanConnectStatus").html("已连接");}else{$("#wanConnectStatus").html("未连接");}
			
			$("#wanMACAddr").html(Aptables.wan.macaddr);
			$("#wanConnectWay").html(Aptables.wan.proto);
			$("#wanIp").html(Aptables.wan.ipaddr);
			$("#wanNetmask").html(Aptables.wan.netmask);
			var wan_dns_len = Aptables.wan.dns; 
			if(wan_dns_len.length > 0){$("#wanDNS").html(wan_dns_len[0]);} else {$("#wanDNS").html("加载中...");}
		});	
	}
	//赋值
	XHR.get('<%=luci.dispatcher.build_url("admin", "wan", "wanOverView")%>',null,function(x, wanconf){
		console.log(wanconf);
		if(wanconf.proto == 'dhcp'){
			//协议赋值
			$('.WANprotocol').val('dhcp');
			$('.PPPOE input').val('');
			$('.staticAddr input').val('');		
			$('.sendName input').val(wanconf.hostname);
			$('.smask input').val('255.255.255.0');
			
			$('.staticAddr').css('display',"none");
	        $('.PPPOE').css('display',"none");
	        $('.DHCPClient').css('display',"block");
			$('.staticAddr input').attr('name','');
			$('.PPPOE input').attr('name','');
		} else if(wanconf.proto == 'static') {
			//协议赋值
			$('.WANprotocol').val('static');
			$('.DHCPClient input').val('');
			$('.PPPOE input').val('');
			$('.IPInput input').val(wanconf.ipaddr);
			$('.smask input').val(wanconf.netmask);
			$('.networkgate input').val(wanconf.gateway);
			$('.borad input').val(wanconf.broadcast);
			$('.DNS input').val(wanconf.dns);
	
			$('.staticAddr').css('display',"block");
	        $('.PPPOE').css('display',"none");
        	$('.DHCPClient').css('display',"none");
			$('.DHCPClient input').attr('name','');
			$('.PPPOE input').attr('name','');
		} else if(wanconf.proto == 'pppoe'){
			//协议赋值
			$('.WANprotocol').val('pppoe');
			$('.DHCPClient input').val('');
			$('.staticAddr input').val('');
			$('.PPOEUser input').val(wanconf.username);
			$('.PPOEPWD input').val(wanconf.password);
			$('.smask input').val('255.255.255.0');
	
			$('.staticAddr').css('display',"none");
	        $('.PPPOE').css('display',"block");
        	$('.DHCPClient').css('display',"none");
			$('.staticAddr input').attr('name','');
			$('.DHCPClient input').attr('name','');
		}
	});	
	$('.WANprotocol').off('change').on('change',function(){
        var protocolVal = $(this).val();
        if(protocolVal == 'dhcp'){
            $('.staticAddr').css('display',"none");
            $('.PPPOE').css('display',"none");
            $('.DHCPClient').css('display',"block");
			$('.staticAddr input').attr('name','');
			$('.PPPOE input').attr('name','');
			$('.DHCPClient input').attr('name','dhostname');
        }else if(protocolVal == 'static'){
            $('.DHCPClient').css('display',"none");
            $('.PPPOE').css('display',"none");
            $('.staticAddr').css('display',"block");
			$('.DHCPClient input').attr('name','');
			$('.PPPOE input').attr('name','');

			$('.IPInput input').attr('name','sip');
			$('.smask input').attr('name','smask');
			$('.networkgate input').attr('name','sgw');
			$('.borad input').attr('name','sbroad');
			$('.DNS input').attr('name','sdns');

        }else if(protocolVal == 'pppoe'){
            $('.staticAddr').css('display',"none");
            $('.DHCPClient').css('display',"none");
            $('.PPPOE').css('display',"block");
			$('.staticAddr input').attr('name','');
			$('.DHCPClient input').attr('name','');

			$('.PPOEUser input').attr('name','puser');
			$('.PPOEPWD input').attr('name','ppwd');
        }
    });
}
function WANprotocol(wanproto){
	console.log($('#protocols').serialize() + "&click=1");
	$.ajax({
    	type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "wan", "wanOverView")%>',
        data: $('#protocols').serialize() + "&click=1"
	}).success(function(message) {
    	layer.open({
		    title: '提示',
		    icon:6
		    ,content: '保存成功'
		});
	}).fail(function(err){
    	console.log(err);
    	layer.open({
		    title: '提示',
		    icon:5
		    ,content: '保存失败'
		});
    });			
}
/**
 * 3. network Ctrl State
**/
function network_ctrl_table(){
	$('.WiFiConfigSelect').html('');
	var timer = false;
	
	if(request){
		request = false;
		network_init();
		if(tab_3!=undefined){clearInterval(tab_3);};
		tab_3 = setInterval(network_init, 5000);
	}
	function network_init(){
		XHR.get('<%=luci.dispatcher.build_url("admin", "status", "info")%>',{status:3},function(x, info){
			var Aptables = info;
			
			var Aplists_net = info.apTable;
			var Aplist_net_doms_1 = $(".net_list_1");
			var Aplist_net_doms_2 = $(".net_list_2");
			var net_mb_list = $(".net_mb_list");
			
			var AP_WiFi_lists = $(".AP_WiFi_lists");
			var AP_WiFi_mb_list = $(".AP_WiFi_list");
			
			if((Aplists_net.length < 1)||(JSON.stringify(Aplists_net) == "{}")){
				$('.netSearch').remove();
				$(".net_mb_search").remove();
				
				$('.AP_WiFi_pc_list').remove();
				$(".AP_WiFi_mb_list").remove();
				//Ap list pc
				var Aplist_net_doms_1_html ='<div class="flex_h align_center list_item_uniform netSearch"><p>暂无AP连接</p><p>-</p><p>-</p></div>';
				var Aplist_net_doms_2_html ='<div class="flex_h align_center list_item_uniform netSearch"><p>-</p><p>-</p><p>-</p></div>';
				Aplist_net_doms_1.append(Aplist_net_doms_1_html);
				Aplist_net_doms_2.append(Aplist_net_doms_2_html);
				//Ap list mb
				var net_mb_dom_html ='<div class="list net_mb_search"><div class="list_item flex_h align_center justify_between"><span>序号</span><span>暂无AP连接</span></div><div class="list_item flex_h align_center justify_between"><span>MAC地址</span><span>-</span></div><div class="list_item flex_h align_center justify_between"><span>IP地址</span><span>-</span></div><div class="list_item flex_h align_center justify_between"><span>在线人数</span><span>-</span></div><div class="list_item flex_h align_center justify_between no-b"><span>状态</span><span>-</span></div><div class="list_item flex_h align_center justify_between no-b"><span></span><div>-</div></div></div>'
	            net_mb_list.append(net_mb_dom_html); 
	            //AP WiFi pc
	            var AP_WiFi_set_doms_html = '<div class="flex_h align_center list_item_uniform AP_WiFi_pc_list"><p>暂无APWiFi</p><p>-</p><p>-</p><p>-</p></div>'
	            AP_WiFi_lists.append(AP_WiFi_set_doms_html);
	            //AP WiFi mb
	            var AP_WiFi_set_mb_html = '<div class="list mb AP_WiFi_mb_list"><div class="list_item flex_h align_center justify_between"><span>MAC地址</span><span>暂无APWiFi</span></div><div class="list_item flex_h align_center justify_between"><span>IP地址</span><span>-</span></div><div class="list_item flex_h align_center justify_between"><span>SSID</span><span>-</span></div><div class="list_item flex_h align_center justify_between no-b"><span>密码</span><span>-</span></div></div>';
	        	AP_WiFi_mb_list.append(AP_WiFi_set_mb_html);
			} else {
				$('.netSearch').remove();
				$(".net_mb_search").remove();
				
				$('.AP_WiFi_pc_list').remove();
				$(".AP_WiFi_mb_list").remove();
				
				for(var t = 0;t < Aplists_net.length; t++){
					//Ap list pc
					var linkings = Aplists_net[t].status == 1 ? '已连接' : '未连接';
					Aplist_net_doms_1_html ='<div class="flex_h align_center list_item_uniform netSearch">'
										+'<p>'+ (t+1) +'</p>'
										+'<p>'+ Aplists_net[t].macaddr +'</p>'
										+'<p><a style="color:rgb(52, 152, 219);" href="http://'+ Aplists_net[t].ip +'">' + Aplists_net[t].ip + '</a></p></div>';
					Aplist_net_doms_2_html ='<div class="flex_h align_center list_item_uniform netSearch">'
										+'<p>'+ Aplists_net[t].ol_num +'</p>'
										+'<p>'+ linkings +'</p>';
										
					var params = "'设备将执行重启，是否确认执行此操作？',{btn: ['确定','取消']},function(){return window.location.href = '<%=luci.dispatcher.build_url("admin", "network", "netControl")%>?rmacaddr=" + Aplists_net[t].macaddr + "';}";
	                Aplist_net_doms_2_html += '<p class=""><button type="primary" size="mini" class="btn_mini reStartExe" status="'+ Aplists_net[t].status +'" mac="'+Aplists_net[t].macaddr+'">执行重启</button></p></div>';
	            	Aplist_net_doms_1.append(Aplist_net_doms_1_html);
					Aplist_net_doms_2.append(Aplist_net_doms_2_html);
					//Ap list mb
					net_mb_dom_html ='<div class="list net_mb_search">'
										+'<div class="list_item flex_h align_center justify_between">'
										+'<span>序号</span>'
										+'<span>'+ (t+1) +'</span>'
										+'</div>'
										+'<div class="list_item flex_h align_center justify_between">'
										+'<span>MAC地址</span>'
										+'<span>'+ Aplists_net[t].macaddr +'</span>'
										+'</div>'
										+'<div class="list_item flex_h align_center justify_between">'
										+'<span>IP地址</span>'
										+'<span>' + Aplists_net[t].ip + '</span>'
										+'</div>'
										+'<div class="list_item flex_h align_center justify_between">'
										+'<span>在线人数</span>'
										+'<span>'+ Aplists_net[t].ol_num +'</span>'
										+'</div>'
										+'<div class="list_item flex_h align_center justify_between no-b">'
										+'<span>状态</span>'
										+'<span>'+ linkings +'</span>'
										+'</div>'
										+'<div class="list_item flex_h align_center justify_between no-b reStartExe">'
										+'<span></span>'
										+'<div><button type="primary" size="mini" class="btn_mini " status="'+ Aplists_net[t].status +'" mac="'+Aplists_net[t].macaddr+'">执行重启</button></div>'
										+'</div>'
										+'</div>';
	            	$(".net_mb_list").append(net_mb_dom_html);	
	            	
	            	//AP WiFi pc
	            	AP_WiFi_set_doms_html = '<div class="flex_h align_center list_item_uniform AP_WiFi_pc_list">'
			                                +    '<p>'+ Aplists_net[t].macaddr +'</p>'
			                                +    '<p>' + Aplists_net[t].ip + '</p>'
			                                +    '<p>' + Aplists_net[t].ssid + '</p>'
			                                +    '<p>' + Aplists_net[t].key + '</p>'
			                                +'</div>';
	            	//AP WiFi mb
	            	AP_WiFi_set_mb_html = '<div class="list mb AP_WiFi_mb_list">'
			                            +    '<div class="list_item flex_h align_center justify_between">'
			                            +        '<span>MAC地址</span>'
			                            +        '<span>'+ Aplists_net[t].macaddr +'</span>'
			                            +    '</div>'
			                            +    '<div class="list_item flex_h align_center justify_between">'
			                            +        '<span>IP地址</span>'
			                            +        '<span>' + Aplists_net[t].ip + '</span>'
			                            +    '</div>'
			                            +    '<div class="list_item flex_h align_center justify_between">'
			                            +        '<span>SSID</span>'
			                            +        '<span>' + Aplists_net[t].ssid + '</span>'
			                            +    '</div>'
			                            +    '<div class="list_item flex_h align_center justify_between no-b">'
			                            +        '<span>密码</span>'
			                            +        '<span>' + Aplists_net[t].key + '</span>'
			                            +    '</div>'
			                            +'</div>';
	            	if(Aplists_net[t].status == 1){
	            		$(".AP_WiFi_lists").append(AP_WiFi_set_doms_html);
				        $(".AP_WiFi_list").append(AP_WiFi_set_mb_html);	
					}
			        if((Aplists_net[t].status == 1)&&(timer == false)){
			            $('.WiFiConfigSelect').append('<option value="'+ Aplists_net[t].macaddr +'">'+ Aplists_net[t].macaddr +'</option>');
			        }
				}
				$(".reStartExe").on("click",function(){
					var macDetail = $(this).attr("mac");
					var status = $(this).attr("status");
					if(status == 1){
						layer.confirm('是否执行重启？', {
							btn: ['确定','取消'] //按钮
						}, function(){
							$.ajax({
			    				type: "post",
					        	url: '<%=luci.dispatcher.build_url("admin", "network", "netControl")%>',
						        data:'rmacaddr='+macDetail
							}).success(function(message) {
							    layer.open({
							    	title: '提示',
									icon:6
						    		,content: '重启成功'
								});
							}).fail(function(err){			
					    		layer.open({
				    				title: '提示'
						    		,content: '重启失败'
								});
							});
						}, function(index){
							layer.close(index);
						});
					}else{
						layer.open({
			            	title: '提示'
			            	,content: '状态未连接'
			        	});
					}
				});
	        }
		    var macLen = $('.WiFiConfigSelect').children().length;
		    if((macLen > 1)&&(timer == false)){
		        $('.WiFiConfigSelect').append('<option value="ff:ff:ff:ff:ff:ff">配置所有AP WiFi</option>')
		    }
			timer = true;
			request = true;
		});
	}
}
function network_ctrl_set(){
	$('.apWifiSettings').off('click').on('click',function () {
	    var WiFiConfigSSIDLen = $('.WiFiConfigSSID input').val().length;
	    var WiFiConfigPWDLen = $('.WiFiConfigPWD input').val().length;
	    var WiFiConfigSSID = $('.WiFiConfigSSID input').val();
	    var WiFiConfigPWD = $('.WiFiConfigPWD input').val();
		var WiFiConfigSelectVal = $('.WiFiConfigSelect').val();	    

		if(WiFiConfigSelectVal != ""){
			if((WiFiConfigSSIDLen=='')||(WiFiConfigPWDLen=='')){
    			layer.open({
	            	title: '提示'
            		,content: 'SSID与密码都不能为空'
        		});
        		return false;
	    	}
	    	if((WiFiConfigSSIDLen=='')||(WiFiConfigPWDLen=='')||(WiFiConfigSSIDLen < 1)||(WiFiConfigPWDLen < 8)){
	        	layer.open({
	            	title: '提示'
	            	,content: 'SSID/密码格式错误，请重新输入'
	        	});
		        $(this).val('');
		        return false;
	    	}else{
	        	if((WiFiConfigPWD != WiFiConfigPWD.replace(/[^\w\.\/]/ig,''))||(WiFiConfigPWD != WiFiConfigPWD.replace(/\s/g,''))||(WiFiConfigSSID != WiFiConfigSSID.replace(/[^\w\.\/]/ig,''))||(WiFiConfigSSID != WiFiConfigSSID.replace(/\s/g,''))){
	            	layer.open({
	                	title: '提示'
		                ,content: 'SSID/密码格式错误，请重新输入'
		            });
	    	        return false;
	        	}else{
					//上网控制-配置-提交-调用
					network_APWiFiConfig_form();
	        	}
	    	}
		} else {
			layer.open({
            	title: '提示'
		        ,content: 'MAC地址不能为空，请选择'
		    });
	    	return false;
		}	    
	});
}
//tool functions
function secondToDate(result) {
	var h = Math.floor(result / 3600) < 10 ? '0'+Math.floor(result / 3600) : Math.floor(result / 3600);
	var m = Math.floor((result / 60 % 60)) < 10 ? '0' + Math.floor((result / 60 % 60)) : Math.floor((result / 60 % 60));
	var s = Math.floor((result % 60)) < 10 ? '0' + Math.floor((result % 60)) : Math.floor((result % 60));
	return result = h + "h : " + m + "m : " + s + "s";
}

//上网控制-配置-表单提交
function network_APWiFiConfig_form(){
	$.ajax({
    	type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "network", "netControl")%>',
        data: $('#apWifiSetForm').serialize()
	}).success(function(message) {
    	layer.open({
	    	title: '提示',
			icon:6
	    	,content: '配置成功'
		});
	}).fail(function(err){			
    	layer.open({
	    	title: '提示'
	    	,content: '配置失败'
		});
	});		
}
/**
 * 4. high Grade Function
 * */
// 高级功能-DHCP-tab
function senior_DHCP(){
	XHR.get('<%=luci.dispatcher.build_url("admin", "advanFunc", "dhcp")%>',null,function(x, info){
		var dynamic_form = info.lan;
		if(info.hasOwnProperty("lan")){
			if(dynamic_form != undefined){
				showDynamic(dynamic_form);							
			}	
		}
	});
	senior_dhcp_init();
	senior_dhcp_tab = setInterval(senior_dhcp_init, 5000);
	function senior_dhcp_init(){
		XHR.get('<%=luci.dispatcher.build_url("admin", "advanFunc", "dhcp")%>',null,function(x, info){
			//console.log(info)
			var hasAlloTables = info.leases;
        	
			var DHCP_pc = $(".DHCP_pc");
			var DHCP_all = $(".DHCP_all");
			
			if((hasAlloTables.length < 1)||(JSON.stringify(hasAlloTables) == "{}")){
                $('.F5ing').remove();
                var hasAlloTables_html = '<div class="flex_h align_center list_item_uniform F5ing"><p>暂无分配的DHCP</p><p>-</p><p>-</p><p>-</p><p>-</p></div>';	
                DHCP_pc.append(hasAlloTables_html);
				$('.mb_F5ing').remove();
                var hasMboTables_html = '<div class="list mb mb_F5ing"><div class="list_item flex_h align_center justify_between"><span>主机名</span> <span>暂无分配的DHCP</span></div> <div class="list_item flex_h align_center justify_between"><span>IPv4地址</span> <span>-</span></div> <div class="list_item flex_h align_center justify_between"><span>MAC地址</span> <span>-</span></div> <div class="list_item flex_h align_center justify_between"><span>连接状态</span> <span>-</span></div> <div class="list_item flex_h align_center justify_between no-b"><span>剩余租期</span> <span>-</span></div></div>';	
                DHCP_all.append(hasMboTables_html);
            }else{
                $('.F5ing').remove();
				$('.mb_F5ing').remove();
                for(var d = 0;d < hasAlloTables.length; d++){
					var dhcp_pc_linking = hasAlloTables[d].status == 1 ? '已连接' : '未连接'
					hasAllo_pc_html ='<div class="flex_h align_center list_item_uniform F5ing">'
										+'<p>'+ hasAlloTables[d].hostname +'</p>'
										+'<p>'+ hasAlloTables[d].ipaddr +'</p>'
										+'<p>'+ hasAlloTables[d].macaddr +'</p>'
										+'<p>'+ dhcp_pc_linking +'</p>';
					var pc_expiresTime = hasAlloTables[d].expires;
					var pc_m = secondToDate(pc_expiresTime);
					hasAllo_pc_html +='<p>'+ pc_m +'</p></div>';
	            	DHCP_pc.append(hasAllo_pc_html);
                	
                	hasAllo_mb_html = '<div class="list mb mb_F5ing">'
                                    +    '<div class="list_item flex_h align_center justify_between">'
                                    +        '<span>主机名</span>'
                                    +        '<span>'+ hasAlloTables[d].hostname +'</span>'
                                    +    '</div>'
                                    +    '<div class="list_item flex_h align_center justify_between">'
                                    +        '<span>IPv4地址</span>'
                                    +        '<span>'+ hasAlloTables[d].ipaddr +'</span>'
                                    +    '</div>'
                                    +    '<div class="list_item flex_h align_center justify_between">'
                                    +        '<span>MAC地址</span>'
                                    +        '<span>'+ hasAlloTables[d].macaddr +'</span>'
                                    +    '</div>'
                                    +    '<div class="list_item flex_h align_center justify_between">'
                                    +        '<span>连接状态</span>'
                                    +        '<span>'+ dhcp_pc_linking +'</span>'
                                    +    '</div>'
                                    +    '<div class="list_item flex_h align_center justify_between no-b">'
                                    +        '<span>剩余租期</span>';
                    var mb_expiresTime = hasAlloTables[d].expires;
    				var mb_m = secondToDate(mb_expiresTime);
                    hasAllo_mb_html +='<span>'+ mb_m +'</span>'
                                    +'</div>'
                                    +'</div>';
	            	DHCP_all.append(hasAllo_mb_html);
                }
            }
		});
	}
}
function senior_DHCP_newadd(){
	$('.DHCPAddAddr').off('click').on('click',function () {
        var hostStr = $('.DHCPstaHost input').val();
        var IPStr = $('.DHCPstaIp4 input').val();
        var MACStr = $('.DHCPstaMac input').val();

        var hostName = /^[-0-9a-zA-Z]*$/g;
        var IPv4Name = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        var MACName = /^([A-Fa-f0-9]{2}:){5}[A-Fa-f0-9]{2}$/;

        if((hostStr=='')||(IPStr=='')||(MACStr=='')){
            layer.open({
                title: '提示信息'
                ,content: '输入内容不能为空,请重新输入'
            });
            return false;
        }else{
            if(!hostName.test(hostStr)){
                layer.open({
                    title: '提示信息'
                    ,content: '输入主机名称格式不正确,请重新输入'
                });
                return false;
            }else if(!IPv4Name.test(IPStr)){
                layer.open({
                    title: '提示信息'
                    ,content: '输入IP地址格式不正确,请重新输入'
                });
                return false;
            }else if(!MACName.test(MACStr)){
                layer.open({
                    title: '提示信息'
                    ,content: '输入MAC地址格式不正确,请重新输入'
                });
                return false;
            }else{
				var reStatic = $('.re_static');
				var reStaticLength = $('.re_static').length;
				var reStaticTest;
				for (var i = 0; i < reStaticLength; i++) {
					reStaticTest = reStatic.eq(i).find('p').eq(1).text();
					if(IPStr == reStaticTest){
						layer.open({
							title: '提示信息'
							,content: '输入的IPv4地址已被占用,请重新输入'
						});
						$('.DHCPstaIp4 input').val('');
						return false;
					}
				}
				static_add = layer.load(0, {shade: false});
                Add_DHCP_Static();
                event.preventDefault();
            }
        }
    });
}
function senior_DHCP_static(){
	senior_dhcp_static_init();
	senior_dhcp_static_tab = setInterval(senior_dhcp_static_init, 5000);
	function senior_dhcp_static_init(){
		XHR.get('<%=luci.dispatcher.build_url("admin", "advanFunc", "dhcp")%>',null,function(x, info){
			//console.log(info)
			var hasHostTables = info.host;
        	
			var DHCP_static_pc = $(".static_pc");
			var DHCP_static_all = $(".static_all");
			
			if((hasHostTables.length < 1)||(JSON.stringify(hasHostTables) == "{}")){
                $('.re_static').remove();
                $('.static_mb').remove();
                var hasHostTables_html = '<div class="flex_h align_center list_item_uniform re_static"><p>暂无</p><p>-</p><p>-</p><p>-</p></div>';
                DHCP_static_pc.append(hasHostTables_html);
                var hasHostTables_mb_html = '<div class="list mb static_mb"><div class="list_item flex_h align_center justify_between"><span>主机名</span><span>暂无</span></div><div class="list_item flex_h align_center justify_between"><span>IPv4地址</span><span>-</span></div><div class="list_item flex_h align_center justify_between"><span>MAC地址</span><span>-</span></div><div class="list_item flex_h align_center justify_between no-b"><span>操作</span><span>-</span></div></div>'; 
                DHCP_static_all.append(hasHostTables_mb_html);
            }else{
                $('.re_static').remove();
                $('.static_mb').remove();
                for(var s = 0;s < hasHostTables.length; s++){
					var dhcp_pc_linking = hasHostTables[s].status == 1 ? '已连接' : '未连接';
					hasHostTables_html ='<div class="flex_h align_center list_item_uniform re_static">'
										+'<p>'+ hasHostTables[s].name +'</p>'
										+'<p>'+ hasHostTables[s].ip +'</p>'
										+'<p>'+ hasHostTables[s].mac +'</p>'
										+'<p><button type="primary" size="mini" class="btn_mini deleteBtn" del="'+ hasHostTables[s].del +'" style="cursor: pointer;">删除</button></p></div>';
	            	DHCP_static_pc.append(hasHostTables_html);
                	
                	hasHostTables_mb_html = '<div class="list mb static_mb">'
                                        +'<div class="list_item flex_h align_center justify_between">'
                                        +    '<span>主机名</span>'
                                        +    '<span>'+ hasHostTables[s].name +'</span>'
                                        +'</div>'
                                        +'<div class="list_item flex_h align_center justify_between">'
                                        +    '<span>IPv4地址</span>'
                                        +    '<span>'+ hasHostTables[s].ip +'</span>'
                                        +'</div>'
                                        +'<div class="list_item flex_h align_center justify_between">'
                                        +    '<span>MAC地址</span>'
                                        +    '<span>'+ hasHostTables[s].mac +'</span>'
                                        +'</div>'
                                        +'<div class="list_item flex_h align_center justify_between no-b">'
                                        +    '<span>操作</span>'
                                        +    '<span><button type="primary" size="mini" class="btn_mini deleteBtn" del="'+ hasHostTables[s].del +'" style="cursor: pointer;">删除</button></span>';
                                        +'</div>'
                                    +'</div>'
	            	DHCP_static_all.append(hasHostTables_mb_html);
                }
            }
			//绑定删除事件
            $('.deleteBtn').on("click",function () {
		            var static_dhcp_del = layer.load(0, {shade: false});
		            var $this = $(this);
		            var dataDel = $(this).attr('del');
		            console.log(dataDel);
		            $.ajax({
		                type: "post",	
		                url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "dhcp")%>',
		                data: "cbid.dhcp.Host.del=" + dataDel
		            }).success(function(message) {
		                //console.log(message);
		                layer.close(static_dhcp_del);
		                $this.parent().parent().remove();
						layer.open({
		                    title: '提示信息'
		                    ,icon:6
		                    ,content: '删除成功'
		                });
		            }).fail(function(err){
		                console.log(err);
		            });
	         }); 
		});
	}
}
function Add_DHCP_Static(){
    $.ajax({
        type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "dhcp")%>',
        data: $('#addStaticItem').serialize()
    }).success(function(message) {
        console.log(message);
        layer.close(static_add); 
		var DHCPAddrLists_pc = $('.static_pc');
        var DHCPAddrLists_mb = $('.static_all');
        var DHCPstaHost = $(".DHCPstaHost input").val();
        var DHCPstaIp4 = $(".DHCPstaIp4 input").val();
        var DHCPstaMac = $(".DHCPstaMac input").val();
        var addHostTables_pc_html ='<div class="flex_h align_center list_item_uniform re_static">'
										+'<p>'+ DHCPstaHost +'</p>'
										+'<p>'+ DHCPstaIp4 +'</p>'
										+'<p>'+ DHCPstaMac +'</p>'
										+'<p><button class="deleteBtn" name="cbid.dhcp.Host.del" style="cursor: pointer;">删除</button></p></div>';
	    DHCPAddrLists_pc.append(addHostTables_pc_html);
		
        var addHostTables_mb_html = '<div class="list mb static_mb">'
                                    +'<div class="list_item flex_h align_center justify_between"><span>主机名</span><span>'
                                    + DHCPstaHost 
                                    +'</span></div><div class="list_item flex_h align_center justify_between"><span>IPv4地址</span><span>'
                                    + DHCPstaIp4 
                                    +'</span></div><div class="list_item flex_h align_center justify_between"><span>MAC地址</span><span>'
                                    + DHCPstaMac 
                                    +'</span></div><div class="list_item flex_h align_center justify_between no-b"><span>操作</span><span><button type="primary" size="mini" class="btn_mini deleteBtn" style="cursor: pointer;">删除</button></span></div></div>';
        DHCPAddrLists_mb.append(addHostTables_mb_html);
		layer.open({
			title: '提示信息'
			,icon:6
        	,content: '添加成功'
			,yes: function(index, layero){
 				$(".DHCPstaHost input").val("");
        		$(".DHCPstaIp4 input").val("");
				$(".DHCPstaMac input").val("");
				layer.close(index);
			}
		});
    }).fail(function(err){
        console.log(err);
    });
}	
function showDynamic(data){
    $('.DHCPStart input').val(data.start);
    $('.DHCPIPNum input').val(data.limit);
    $('.DHCPLease input').val(data.leasetime);
}
function DHCPSaveApply(){
	$('.DHCPSaveApply').off('click').on('click',function(){
		DHCPSaveApply = layer.load(0, {shade: false});
		var test = $('.DHCPLease input').val();
		var reg1 = /^[0-9h]+$/;
		if(!reg1.test(test)){
		    layer.close(DHCPSaveApply);
		    layer.open({
		        title: '提示信息'
		        ,content: '请输入正确的时间长度'
		    });
		    return false;
		}else{
		    if((test.substr(test.length-1,1) == 'h')||(test.substr(test.length-1,1) == 'm')){
		        DHCPSaveApply_post();
		        event.preventDefault();
		    }else {
		        layer.close(DHCPSaveApply);
		        layer.open({
		            title: '提示信息'
		            ,content: '请输入正确的时间长度'
		        });
		        return false;
		    }
		}
	});
}
function DHCPSaveApply_post(){
	$.ajax({
	    type: "post",
	    url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "dhcp")%>',
	    data:  $('#dynamicAddrForm').serialize()
    }).success(function(message) {
		layer.close(DHCPSaveApply);
			layer.open({
        	title: '提示信息'
        	,content: '设置成功'
    	});
	}).fail(function(err){
	})
}
//高级功能-防火墙-pc
function high_grade_function_firewall(){
	//执行第一次
	firewall_tab();
	macAddrFilterTab();
	domainFilterTab();
	DMZFilterTab();
	
	//只执行一次
	senior_fw_port_add();
	addMACFilterValid_pc();
	addDomainFilterBtn_pc();
	addDMZBtn_pc();
	
	//总的定时器
	high_grade_function_firewall_interval = setInterval(function(){
		firewall_tab();
		macAddrFilterTab();
		domainFilterTab();
		DMZFilterTab();
	},5000);
}
//高级功能-防火墙-mb
function high_grade_function_firewall_mb(){
	//执行第一次
	firewall_tab_mb();
	macAddrFilterTab();
	domainFilterTab();
	DMZFilterTab();
	
	//只执行一次
	senior_fw_port_add_mb();
	addMACFilterValid_mb();
	addDomainFilterBtn_mb();
	addDMZBtn_mb();
	
	//总的定时器
	high_grade_function_firewall_interval_mb = setInterval(function(){
		firewall_tab_mb();
		macAddrFilterTab();
		domainFilterTab();
		DMZFilterTab();
	},5000);
}
//高级功能-防火墙-端口转发-展示
function firewall_tab(){
	XHR.get('<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',null,function(x, info){
		console.log(info);
		var fw_pc = $(".fw_pc");
		var fWTables = info.forwardstab;
		var firewall_port_show;
		if((fWTables.length < 1) || JSON.stringify(fWTables ) == "{}"){
			$('.pc_fwSearch').remove();
			firewall_port_show = '<div class="flex_h align_center list_item_uniform no-h no-b pc_fwSearch"><p>暂无端口转发...</p><div><p class="w80"></p></div><p></p><p></p><div></div><div></div></div>';
			fw_pc.append(firewall_port_show);
		} else {
			$('.pc_fwSearch').remove();
			for (var i = 0; i < fWTables.length; i++) {
				var status;
				if(fWTables[i].enabled == "1"){
					status = '<div><input type="checkbox" class="switch_1" name="' + fWTables[i].sname + '" checked></div>';
				}else{
					status = '<div><input type="checkbox" class="switch_1" name="' + fWTables[i].sname + '"></div>';
				};
				firewall_port_show = '<div class="flex_h align_center list_item_uniform no-h no-b pc_fwSearch" name="'
										+fWTables[i].sname
	    								+'"><p>'+ fWTables[i].name +'</p>'
	        							+'<div><p class="w80">'+ fWTables[i].matchstr +'</p></div>'
	        							+'<p>'+ fWTables[i].forwardstr +'</p>'
	        							+ status
	        							+ '<div><button type="button" class="el-button el-button--info el-button--mini" onclick="moveUp(this)"><span><i class="el-icon-arrow-up"></i></span></button> <button type="button" class="el-button el-button--info el-button--mini" onclick="moveDown(this)"><span><i class="el-icon-arrow-down"></i></span></button></div>'
	        							+ '<div><button type="button" size="mini" class="btn_mini deleteBtnfw" del="'
	        							+ fWTables[i].sname
	        							+ '" style="cursor: pointer;">删除</button></div></div>';
				fw_pc.append(firewall_port_show);				
			};
			$('.deleteBtnfw').off('click').on('click',function(){
				var firewall_port_del = layer.load(0, {shade: false});
				var $this = $(this);
        		var dataDel = $(this).attr('del');
        		console.log(dataDel);
        		var formData = 'forwards_sname=' + dataDel + '&but_remforwards=1';
        		$.ajax({
        			type: "post",	
        			url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
        			data: formData
        		}).success(function(message) {
        			layer.close(firewall_port_del);
        			$this.parent().parent().remove();
					layer.open({
            			title: '提示信息'
            			,icon:6
            			,content: '删除成功'
           			});
        		}).fail(function(err){
            			console.log(err);
        		});
			})
			//高级功能-防火墙-端口转发-开关切换
			$('.switch_1').off('change').on('change',function(){
				var $this = $(this);
				var forwards_sname = $this.attr("name");
				var but_openforwards = $this.prop('checked')? 1 : 0;
				var formData = 'forwards_sname='+forwards_sname+'&but_openforwards='+but_openforwards;
				senior_fw_port_switch (formData);
			})
		}
	});			
}

//高级功能-防火墙-端口转发-开关切换-post
function senior_fw_port_switch (formModal) {
	var senior_fw_port_switch_layer = layer.load(0, {shade: false});
    $.ajax({
        type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
        data: formModal
    }).success(function(message) {
        console.log(message,formModal);
        layer.close(senior_fw_port_switch_layer);
    }).fail(function(err){
        console.log(err,formModal);
    })
}
//高级功能-防火墙-端口转发-添加
function senior_fw_port_add(){
	$('.addPortBtn_pc').off('click').on('click',function () {
		$('.addPortClick').val(1);
		var portName = $('.forwards_add_name_pc input').val();
		var outport = $('.forwards_add_src_dport_pc input').val();
		var innerIP = $('.forwards_add_dest_ip_pc input').val();
		var innerport = $('.forwards_add_dest_port_pc input').val();

		var reg = /(^[1-9]\d*$)/;
		var regIP = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
		
		if((portName=='')||(outport=='')||(innerIP=='')||(innerport=='')){
    		layer.open({
        		title: '提示信息'
                ,content: '输入内容不能为空,请重新输入'
            });
            return false;
        }else{
			if((!reg.test(outport))||(!reg.test(innerport))){
				layer.open({
        			title: '提示信息'
                	,content: '输入内容不是正整数,请重新输入'
            	});
            	return false;
			} else if(!regIP.test(innerIP)){
				layer.open({
        			title: '提示信息'
                	,content: '请输入正确的IP地址'
            	});
            	return false;				
			} else{
				if((outport > 65535)||(innerport > 65535)){
					layer.open({
	        			title: '提示信息'
	    	        	,content: '请输入正确的端口值'
    	        	});
	            	return false;	
				} else{
					$('.forwards_add_name').val();
    			    addPortPost();
					$('.forwards_add_name_pc input').val('');
					$('.forwards_add_src_dport_pc input').val('');
					$('.forwards_add_dest_ip_pc input').val('');
					$('.forwards_add_dest_port_pc input').val('');
			        event.preventDefault();
				}				
			}			
		}		
    });
}
//高级功能-防火墙-端口转发-添加-post
function addPortPost () {
    $.ajax({
        type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
        data: $('#addPortChange_pc').serialize() + '&but_addforwards=0'
    }).success(function(message) {
        layer.open({
		    title: '提示',
		    icon:6
		    ,content: '配置成功'
		});
    }).fail(function(err){
    	console.log(err,dataForm);
		layer.open({
		    title: '提示',
			icon:5
		    ,content: '配置失败'
		});
    })
}	
// 高级功能-防火墙-端口转发-展示-手机端
function firewall_tab_mb(){
	XHR.get('<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',null,function(x, info){
		console.log(info);
		var fw_all = $(".fw_all");
		var fWTables = info.forwardstab;
		var firewall_port_show;
		if((fWTables.length < 1) || JSON.stringify(fWTables ) == "{}"){
			$('.mb_fwsearch').remove();
			firewall_port_show = '<div class="list mb mb_fwsearch"><div class="list_item flex_h align_center justify_between"><span>暂无端口转发...</span> <span></span></div></div>';
			fw_all.append(firewall_port_show);
		} else {
			$('.mb_fwsearch').remove();
			for (var i = 0; i < fWTables.length; i++) {
				var status;
				if(fWTables[i].enabled == "1"){
					status = '<div class="list_item flex_h align_center justify_between"><span>开关</span><input type="checkbox" class="switch_1" name="' + fWTables[i].sname + '" checked></div>';
				}else{
					status = '<div class="list_item flex_h align_center justify_between"><span>开关</span><input type="checkbox" class="switch_1" name="' + fWTables[i].sname + '"></div>';
				};							
	        	firewall_port_show ='<div class="list mb mb_fwsearch" name="'
	        						+fWTables[i].sname
	        						+'"><div class="list_item flex_h align_center justify_between"><span>名字</span><span>'
	        						+ fWTables[i].name
	        						+'</span></div><div class="list_item flex_h align_center justify_between"><span>匹配规则</span><span>'
	        						+ fWTables[i].matchstr
	        						+'</span></div><div class="list_item flex_h align_center justify_between"><span>转发到</span><span>'
	        						+ fWTables[i].forwardstr
	        						+'</span></div>'
	        						+ status
	        						+'<div class="list_item flex_h align_center justify_between"><span>排序</span><button type="button" class="el-button el-button--info el-button--mini btn_up_mb" onclick="moveUp(this)"><span><i class="el-icon-arrow-up"></i></span></button><button type="button" class="el-button el-button--info el-button--mini btn_down_mb" onclick="moveDown(this)"><span><i class="el-icon-arrow-down"></i></span></button></div><div class="list_item flex_h align_center justify_between no-b"><span>操作</span> <button type="button" class="el-button el-button--info el-button--small deleteBtnfw_mb" del="'
	        						+ fWTables[i].sname
	        						+'"><span>删除</span></button></div></div>';
				fw_all.append(firewall_port_show);
			};
			//高级功能-防火墙-端口转发-删除
			$('.deleteBtnfw_mb').off('click').on('click',function(){
				var firewall_port__del = layer.load(0, {shade: false});
				var $this = $(this);
	            var dataDel = $(this).attr('del');
	            console.log(dataDel);
	            var formData = 'forwards_sname=' + dataDel + '&but_remforwards=1';
	            $.ajax({
	                type: "post",	
	                url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
	                data: formData
	            }).success(function(message) {
	                //console.log(message);
	                layer.close(firewall_port__del);
	                $this.parent().parent().remove();
					layer.open({
	                    title: '提示信息'
	                    ,icon:6
	                    ,content: '删除成功'
	                });
	            }).fail(function(err){
	                console.log(err);
	            });
			})
			//高级功能-防火墙-端口转发-开关切换
			$('.switch_1').off('change').on('change',function(){
				var $this = $(this);
				var forwards_sname = $this.attr("name");
				var but_openforwards = $this.prop('checked')? 1 : 0;
				var formData = 'forwards_sname='+forwards_sname+'&but_openforwards='+but_openforwards;
				senior_fw_port_switch (formData);
			})
		}
	});		
}
//高级功能-防火墙-端口转发-添加-手机端
function senior_fw_port_add_mb(){
	$('.addPortBtn_mb').off('click').on('click',function () {
		$('.addPortClick').val(1);
		var portName = $('.forwards_add_name_mb input').val();
		var outport = $('.forwards_add_src_dport_mb input').val();
		var innerIP = $('.forwards_add_dest_ip_mb input').val();
		var innerport = $('.forwards_add_dest_port_mb input').val();

		var reg = /(^[1-9]\d*$)/;
		var regIP = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
		
		if((portName=='')||(outport=='')||(innerIP=='')||(innerport=='')){
    		layer.open({
        		title: '提示信息'
                ,content: '输入内容不能为空,请重新输入'
            });
            return false;
        }else{
			if((!reg.test(outport))||(!reg.test(innerport))){
				layer.open({
        			title: '提示信息'
                	,content: '输入内容不是正整数,请重新输入'
            	});
            	return false;
			} else if(!regIP.test(innerIP)){
				layer.open({
        			title: '提示信息'
                	,content: '请输入正确的IP地址'
            	});
            	return false;				
			} else{
				if((outport > 65535)||(innerport > 65535)){
					layer.open({
            			title: '提示信息'
        	        	,content: '请输入正确的端口值'
    	        	});
	            	return false;	
				} else{
					$('.forwards_add_name').val();
    			    addPortPostMb();
				$('.forwards_add_name_mb input').val('');
					$('.forwards_add_src_dport_mb input').val('');
					$('.forwards_add_dest_ip_mb input').val('');
					$('.forwards_add_dest_port_mb input').val('');
			        event.preventDefault();
				}				
			}			
		}		
    });
}
//高级功能-防火墙-端口转发-添加-手机端-post
function addPortPostMb () {
    $.ajax({
        type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
        data: $('#addPortChange_mb').serialize() + '&but_addforwards=0'
    }).success(function(message) {
        layer.open({
		    title: '提示',
		    icon:6
		    ,content: '配置成功'
		});
    }).fail(function(err){
    	console.log(err,dataForm);
		layer.open({
		    title: '提示',
			icon:5
		    ,content: '配置失败'
		});
    })
}
/*
**封装列表上移、下移函数
**传入参数为当前按钮
*/
function moveUp(_a){
    var _row = _a.parentNode.parentNode;		
    //如果不是第一行，则与上一行交换顺序
    var _node = _row.previousSibling;
    while(_node && _node.nodeType != 1){
        _node = _node.previousSibling;
    }
    if(_node){
        swapNode(_row,_node);
    }else {
		layer.open({
			title: '信息提示'
			,icon: 6
			,content: '已经是最高优先级啦'
		});  
	}
}
function moveDown(_a){
    var _row = _a.parentNode.parentNode;
    //如果不是最后一行，则与下一行交换顺序
    var _node = _row.nextSibling;
    while(_node && _node.nodeType != 1){
        _node = _node.nextSibling;
    }
    if(_node){
        swapNode(_row,_node);
    }else {
		layer.open({
			title: '信息提示'
			,icon: 6
			,content: '已经是最低优先级啦'
		});  
	}
}
function swapNode(node1,node2){
    //获取父结点
    var _parent = node1.parentNode;
    //获取两个结点的相对位置
    var _t1 = node1.nextSibling;
    var _t2 = node2.nextSibling;
    //将node2插入到原来node1的位置
    if(_t1)_parent.insertBefore(node2,_t1);
    else _parent.appendChild(node2);
    //将node1插入到原来node2的位置
    if(_t2)_parent.insertBefore(node1,_t2);
    else _parent.appendChild(node1);

	var fwNumLength = $('.pc_fwSearch').length;
	if(fwNumLength == 1){
		var fwNum = $('.mb_fwsearch');
		console.log(fwNum.length,"1")
	}else{
		var fwNum = $('.pc_fwSearch');
		console.log(fwNum.length,"2")
	};
	var order = [];
	var orderBtn = 1;
	for(var j = 0; j < fwNum.length; j++){				
		var s= $(fwNum[j]).attr('name');
		order.push(s);					
	}
	console.log(order);
	order.reverse();
	var orderStr = order.join(":");
	console.log(orderStr);
	var orderform = 'forwards_snames=' + orderStr + '&but_reoforwards=' + orderBtn ;
  	senior_fw_port_switch(orderform);
}

//高级功能-防火墙-MAC地址过滤-表格
function macAddrFilterTab(){
	XHR.get('<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',null,function(x, info){
		//console.log(info);
		var macAddrList_pc = $(".macAddrList_pc");
		var macAddrList_all = $(".macAddrList_all");
		var macftTab = info.macfiltertab;
		var macft_show_pc;
		var macft_show_mb;

		if((macftTab.length < 1) || JSON.stringify(macftTab ) == "{}"){
			$('.macfilterSearch_pc').remove();	
			$('.macfilterSearch_mb').remove();
			macft_show_pc = '<div class="flex_h align_center list_item_uniform macfilterSearch_pc"><div class="w100"><p>暂无需要过滤的MAC地址</p></div><div class="w200"><p>-</p></div><div class="w200"><p>-</p></div><div class="w200"><p>-</p></div><div class="w100"><el-button type="info" size="small" class="macAddrDel">-</el-button></div></div>';
			macft_show_mb = '<div class="list mb macfilterSearch_mb"><div class="list_item flex_h align_center justify_between"><span>源端口</span><span>暂无需要过滤的MAC地址</span></div><div class="list_item flex_h align_center justify_between"><span>目的端口</span><span>-</span></div><div class="list_item flex_h align_center justify_between"><span>源MAC地址</span><span>-</span></div><div class="list_item flex_h align_center justify_between"><span>动作</span><span>-</span></div><div class="list_item flex_h align_center justify_between no-b"><span>操作</span><el-button type="info" size="small" class="macAddrDel">删除</el-button></div></div>'
			macAddrList_pc.append(macft_show_pc);
			macAddrList_all.append(macft_show_mb);
		} else {
			$('.macfilterSearch_pc').remove();
			$('.macfilterSearch_mb').remove();
			for (var i = 0; i < macftTab.length; i++) {
				macft_show_pc = '<div class="flex_h align_center list_item_uniform macfilterSearch_pc">'
				            +'<div class="w100"><p>'+ macftTab[i].src +'</p></div>'
				            +'<div class="w200"><p>'+ macftTab[i].dest +'</p></div>'
				            +'<div class="w200"><p>'+ macftTab[i].src_mac +'</p></div>'
				            +'<div class="w200"><p>'+ macftTab[i].target +'</p></div>'
				           	+'<div class="w100"><button type="button" class="el-button el-button--info el-button--small macAddrDel" del="'+ macftTab[i].sname +'"><span>删除</span></button></div>'
        					+'</div>'
        		macAddrList_pc.append(macft_show_pc);
	        
				macft_show_mb = '<div class="list mb macfilterSearch_mb">'
				            +'<div class="list_item flex_h align_center justify_between">'
				            +'<span>源端口</span>'
				            +'<span>'+ macftTab[i].src +'</span>'
				            +'</div>'
				            +'<div class="list_item flex_h align_center justify_between">'
				            +'<span>目的端口</span>'
				            +'<span>'+ macftTab[i].dest +'</span>'
				            +'</div>'
				            +'<div class="list_item flex_h align_center justify_between">'
				            +'<span>源MAC地址</span>'
				            +'<span>'+ macftTab[i].src_mac +'</span>'
				            +'</div>'
				            +'<div class="list_item flex_h align_center justify_between">'
				            +'<span>动作</span>'
				            +'<span>'+ macftTab[i].target +'</span>'
				            +'</div>'
				            +'<div class="list_item flex_h align_center justify_between no-b">'
				            +'<span>操作</span>'
				            +'<button type="button" class="el-button el-button--info el-button--small macAddrDel" del="'+ macftTab[i].sname +'"><span>删除</span></button>'
				            +'</div>'
				        	+'</div>';
        		macAddrList_all.append(macft_show_mb);
			};
			$('.macAddrDel').off('click').on('click',function(){
				var macAddr_del = layer.load(0, {shade: false});
				var $this = $(this);
	            var dataDel = $(this).attr('del');
	            var formDate = 'forwards_sname=' + dataDel + '&but_remforwards=1';
	            $.ajax({
	                type: "post",	
	                url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
	                data: formDate
	            }).success(function(message) {
	                //console.log(message);
	                layer.close(macAddr_del);
	                $this.parent().parent().remove();
					layer.open({
	                    title: '提示信息'
	                    ,icon:6
	                    ,content: '删除成功'
	                });
	            }).fail(function(err){
	                console.log(err);
	            });
			});
		}
	});	
}
//高级功能-防火墙-MAC地址过滤-添加-校验-pc
function addMACFilterValid_pc(){
	$('.addMACFilterBtn_pc').off("click").on('click',function () {	
		var macreg = /^([A-Fa-f0-9]{2}:){5}[A-Fa-f0-9]{2}$/;
		var macInput = $('.macfilter_add_src_mac_pc input').val();
		if(macInput==''){
	        layer.open({
		        title: '提示信息'
		        ,content: '输入内容不能为空,请重新输入'
	        });
	        return false;
	    }
		if(!macreg.test(macInput)){
			layer.open({
				title: '提示信息'
				,content: '请输入合法的MAC地址'
			});
			return false;
		} else {
			var macBefore = $('.macfilterSearch_pc');
			var macBeforeLength = $('.macfilterSearch_pc').length;
			var macTest;
			for (var i = 0; i < macBeforeLength; i++) {
				macTest = macBefore.eq(i).find('div').eq(2).find('p').text();
				if(macInput == macTest){
					layer.open({
						title: '提示信息'
						,content: '输入的MAC地址已被占用,请重新输入'
					});
					$('.macfilter_add_src_mac_pc input').val('');
					return false;
				}
			}
			macFilterAdd_load_pc = layer.load(0, {shade: false});
			macFilterAdd_pc();
			$('.macfilter_add_src_mac_pc input').val('');
    		event.preventDefault();
		}
	});
}
//高级功能-防火墙-MAC地址过滤-添加-校验-mb
function addMACFilterValid_mb(){
	$('.addMACFilterBtn_mb').off('click').on('click',function () {			
		var macreg = /^([A-Fa-f0-9]{2}:){5}[A-Fa-f0-9]{2}$/;
		var macInput = $('.macfilter_add_src_mac_mb input').val();
		if(macInput==''){
	        layer.open({
	            title: '提示信息'
	            ,content: '输入内容不能为空'
	        });
	        return false;
	    }
		if(!macreg.test(macInput)){
			layer.open({
				title: '提示信息'
				,content: '请输入合法的MAC地址'
			});
			return false;
		} else {
			var macBefore = $('.macfilterSearch_mb');
			var macBeforeLength = $('.macfilterSearch_mb').length;
			var macTest;
			for (var i = 0; i < macBeforeLength; i++) {
				macTest = macBefore.eq(i).find('div').eq(2).find('span').eq(1).text();
				if(macInput == macTest){
					layer.open({
						title: '提示信息'
						,content: '输入的MAC地址已被占用，请重新输入'
					});
					$('.macfilter_add_src_mac_mb input').val('');
					return false;
				}
			}
			macFilterAdd_load_mb = layer.load(0, {shade: false});
			macFilterAdd_mb();
			$('.macfilter_add_src_mac_mb input').val('');
    		event.preventDefault();
		}
	});
}
//高级功能-防火墙-MAC地址过滤-添加-post-pc
function macFilterAdd_pc () {
    $.ajax({
        type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
        data: $('#macAddForm_pc').serialize() + "&but_addmacfilter=1"
    }).success(function(message) {
    	layer.close(macFilterAdd_load_pc);
			layer.open({
	        title: '提示信息'
	        ,icon:6
	        ,content: '添加成功'
	    });
    }).fail(function(err){
    	layer.close(macFilterAdd_load_pc);
        console.log(err);
    })
}
//高级功能-防火墙-MAC地址过滤-添加-post-mb
function macFilterAdd_mb () {
    $.ajax({
        type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
        data: $('#macAddForm_mb').serialize() + "&but_addmacfilter=1"
    }).success(function(message) {
    	layer.close(macFilterAdd_load_mb);
			layer.open({
	        title: '提示信息'
	        ,icon:6
	        ,content: '添加成功'
	    });
    }).fail(function(err){
        console.log(err);
    })
}

//高级功能-防火墙-域名过滤-表格
function domainFilterTab(){
	XHR.get('<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',null,function(x, info){
		//console.log(info);
		var fWDomainFilter_all = $(".fWDomainFilter_all");
		var fWDomainFilter_pc = $(".fWDomainFilter_pc")

		var fWDomainTab = info.dntab;
		var domain_show_pc;
		var domain_show_mb;

		if((fWDomainTab.length < 1) || JSON.stringify(fWDomainTab) == "{}"){
			$('.DFilterSearch_pc').remove();	
			$('.DFilterSearch_mb').remove();
			
			domain_show_pc = '<div class="flex_h align_center list_item_uniform DFilterSearch_pc"><p>暂无需要过滤的域名</p><p>-</p><div class="w100"><el-button type="info" size="small" class="dnDelete">-</el-button></div></div>';
			domain_show_mb = '<div class="list mb DFilterSearch_mb"><div class="list_item flex_h align_center justify_between"><span>域名</span><span>暂无需要过滤的域名</span></div><div class="list_item flex_h align_center justify_between"><span>动作</span><span></span></div><div class="list_item flex_h align_center justify_between no-b"><span>操作</span><el-button type="info" size="small" class="dnDelete">-</el-button></div></div>'
			
			fWDomainFilter_pc.append(domain_show_pc);
			fWDomainFilter_all.append(domain_show_mb);
		} else {
			$('.DFilterSearch_pc').remove();	
			$('.DFilterSearch_mb').remove();
				
			for (var i = 0; i < fWDomainTab.length; i++) {
				domain_show_pc = '<div class="flex_h align_center list_item_uniform DFilterSearch_pc">'
								+'<p>'+ fWDomainTab[i].dn +'</p>'
								+'<p>'+ fWDomainTab[i].target +'</p>'
								+'<div class="w100">'
								+	'<button type="button" class="el-button el-button--info el-button--small dnDelete" del="'+ fWDomainTab[i].dn +'"><span>删除</span></button>'
								+'</div>';
		        fWDomainFilter_pc.append(domain_show_pc);
		        
				domain_show_mb = '<div class="list mb DFilterSearch_mb">'
			                        +'<div class="list_item flex_h align_center justify_between">'
			                        +'<span>域名</span>'
			                        +'<span>'+ fWDomainTab[i].dn +'</span>'
			                        +'</div>'
			                        +'<div class="list_item flex_h align_center justify_between">'
			                        +'<span>动作</span>'
			                        +'<span>'+ fWDomainTab[i].target +'</span>'
			                        +'</div>'
			                        +'<div class="list_item flex_h align_center justify_between no-b">'
			                        +'<span>操作</span>'
			                        +'<button type="button" class="el-button el-button--info el-button--small dnDelete" del="'+ fWDomainTab[i].dn +'"><span>删除</span></button>'
			                        +'</div>'
                        			+'</div>';
		        fWDomainFilter_all.append(domain_show_mb);	        
	        
			};
			$('.dnDelete').off('click').on('click',function(){
				var dn_del = layer.load(0, {shade: false});
				var $this = $(this);
	            var dataDel = $(this).attr('del');
	            var formDate ='but_remdn=1&dn_rem_str=' + dataDel; 
	            $.ajax({
	                type: "post",	
	                url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
	                data: formDate
	            }).success(function(message) {
	                //console.log(message);
	                layer.close(dn_del);
	                $this.parent().parent().remove();
					layer.open({
	                    title: '提示信息'
	                    ,icon:6
	                    ,content: '删除成功'
	                });
	            }).fail(function(err){
	                console.log(err);
	            });
			});
		}
	});	
}

//高级功能-防火墙-域名过滤-添加-校验-pc
function addDomainFilterBtn_pc(){
	$('.addDomainFilterBtn_pc').off('click').on('click',function () {			
		var dnInput = $('.dn_add_str_pc input').val();
		if(dnInput==''){
	        layer.open({
	            title: '提示信息'
	            ,content: '输入内容不能为空,请重新输入'
	        });
	        return false;
	    }
		var DFilterSearch_pc_test = $('.DFilterSearch_pc');
		var DFilterSearch_pc_test_val;
		for (var i = 0; i < DFilterSearch_pc_test.length; i++) {
			DFilterSearch_pc_test_val = DFilterSearch_pc_test.eq(i).find('p').eq(0).text();
			if(DFilterSearch_pc_test_val == dnInput){
				layer.open({
		            title: '提示信息'	
		            ,content: '该域名已过滤'
		        });
		        return false;
			}
		}
    	dnFilterAdd_load_pc = layer.load(0, {shade: false});
        dnFilterAdd_pc();
		event.preventDefault();			
	});	
}
//高级功能-防火墙-域名过滤-添加-校验-mb
function addDomainFilterBtn_mb(){
	$('.addDomainFilterBtn_mb').off('click').on('click',function () {
		var dnInput = $('.dn_add_str_mb input').val();
		if(dnInput==''){
	        layer.open({
	            title: '提示信息'
	            ,content: '输入内容不能为空,请重新输入'
	        });
	        return false;
	    }
		var DFilterSearch_mb_test = $('.DFilterSearch_mb');
		var DFilterSearch_mb_test_val;
		for (var i = 0; i < DFilterSearch_mb_test.length; i++) {
			DFilterSearch_mb_test_val = DFilterSearch_mb_test.eq(i).find('div').eq(0).find('span').eq(1).text();
			if(DFilterSearch_mb_test_val == dnInput){
				layer.open({
		            title: '提示信息'
		            ,content: '该域名已过滤'
		        });
		        return false;
			}
		}
  		dnFilterAdd_load_mb = layer.load(0, {shade: false});
        dnFilterAdd_mb();
    	event.preventDefault();
	});	
}
//高级功能-防火墙-域名过滤-添加-post-pc
function dnFilterAdd_pc () {
    $.ajax({
        type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
        data: $('#fWDomainFilter_pc').serialize() + "&but_adddn=1"
    }).success(function(message) {
    	layer.close(dnFilterAdd_load_pc);
			layer.open({
	        title: '提示信息'
	        ,icon:6
	        ,content: '添加成功'
	    });
	    $('.dn_add_str_pc input').val('');
    }).fail(function(err){
        console.log(err);
    })
}
//高级功能-防火墙-域名过滤-添加-post-mb
function dnFilterAdd_mb () {	
    $.ajax({
        type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
        data: $('#fWDomainFilter_mb').serialize() + "&but_adddn=1"
    }).success(function(message) {
    	layer.close(dnFilterAdd_load_mb);
			layer.open({
	        title: '提示信息'
	        ,icon:6
	        ,content: '添加成功'
	    });
	    $('.dn_add_str_mb input').val('');
    }).fail(function(err){
        console.log(err);
    })
}
//高级功能-防火墙-DMZ过滤-表格
function DMZFilterTab(){
	XHR.get('<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',null,function(x, info){
		//console.log(info);
		var fWDMZ_all = $(".fWDMZ_all");
		var fWDMZ_pc = $(".fWDMZ_pc")

		var DMZTab = info.dmztab;
		var fWDMZ_show_pc;
		var fWDMZ_show_mb;

		if((DMZTab.length < 1) || JSON.stringify(DMZTab) == "{}"){
			$('.DMZSearch_pc').remove();	
			$('.DMZSearch_mb').remove();
			
			fWDMZ_show_pc = '<div class="flex_h align_center list_item_uniform DMZSearch_pc"><div class="w100"><p>暂无需要过滤的DMZ</p></div><p>-</p><p>-</p><p>-</p><div class="w100"><el-button type="info" size="small" class="dmzDelete">-</el-button></div></div>';
			fWDMZ_show_mb = '<div class="list mb DMZSearch_mb"><div class="list_item flex_h align_center justify_between"><span>源端口</span><span>暂无需要过滤的DMZ</span></div><div class="list_item flex_h align_center justify_between"><span>协议</span><span>-</span></div><div class="list_item flex_h align_center justify_between"><span>目的IP</span><span>-</span></div><div class="list_item flex_h align_center justify_between"><span>动作</span><span>-</span></div><div class="list_item flex_h align_center justify_between no-b"><span>操作</span><el-button type="info" size="small" class="dmzDelete">-</el-button></div></div>';
			
			fWDMZ_pc.append(fWDMZ_show_pc);
			fWDMZ_all.append(fWDMZ_show_mb);
		} else {
			$('.DMZSearch_pc').remove();	
			$('.DMZSearch_mb').remove();
				
			for (var i = 0; i < DMZTab.length; i++) {
				fWDMZ_show_pc = '<div class="flex_h align_center list_item_uniform DMZSearch_pc"><div class="w100"><p>'
								+ DMZTab[i].src 
								+'</p></div><p>'
								+ DMZTab[i].proto 
								+'</p><p>'
								+ DMZTab[i].dest_ip 
								+'</p><p>'
								+ DMZTab[i].target 
								+'</p><div class="w100"><button type="button" class="el-button el-button--info el-button--small dmzDelete" del="'
		                        + DMZTab[i].sname 
		                        +'"><span>删除</span></button></div></div>';
	        	fWDMZ_pc.append(fWDMZ_show_pc);
				fWDMZ_show_mb = '<div class="list mb DMZSearch_mb"><div class="list_item flex_h align_center justify_between"><span>源端口</span><span>'
								+ DMZTab[i].src 
								+'</span></div><div class="list_item flex_h align_center justify_between"><span>协议</span><span>'
								+ DMZTab[i].proto 
								+'</span></div><div class="list_item flex_h align_center justify_between"><span>目的IP</span><span>'
								+ DMZTab[i].dest_ip 
								+'</span></div><div class="list_item flex_h align_center justify_between"><span>动作</span><span>'
								+ DMZTab[i].target 
								+'</span></div><div class="list_item flex_h align_center justify_between no-b"><span>操作</span><button type="button" class="el-button el-button--info el-button--small dmzDelete" del="'
								+ DMZTab[i].sname 
								+'"><span>删除</span></button></div></div>';
	        	fWDMZ_all.append(fWDMZ_show_mb);	        
			};
			$('.dmzDelete').off('click').on('click',function(){
				var DMZ_del = layer.load(0, {shade: false});
				var $this = $(this);
		        var dataDel = $(this).attr('del');
		        var formDate ='but_remdmz=1&dmz_sname=' + dataDel;
		        $.ajax({
		            type: "post",	
		            url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
		            data: formDate
		        }).success(function(message) {
		            //console.log(message);
		            layer.close(DMZ_del);
		            $this.parent().parent().remove();
					layer.open({
		                title: '提示信息'
		                ,icon:6
		                ,content: '删除成功'
		            });
		        }).fail(function(err){
		            console.log(err);
		        });
			});
		}
	});	
}
//高级功能-防火墙-DMZ过滤-添加-校验-pc
function addDMZBtn_pc(){
	$('.addDMZBtn_pc').off("click").on('click',function () {			
		var dmzInput = $('.dmz_add_ip_pc input').val();
		var regIPv4 = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
		if(dmzInput==''){
            layer.open({
                title: '提示信息'
                ,content: '输入内容不能为空,请重新输入'
            });
            return false;
        }
		if(!regIPv4.test(dmzInput)){
			layer.open({
            	title: '提示信息'
            	,content: '请输入正确的IP地址'
        	});
        	return false;				
		} else {
			var DMZSearch_pc_test = $('.DMZSearch_pc');
			var DMZSearch_pc_test_val;
			for (var i = 0; i < DMZSearch_pc_test.length; i++) {
				DMZSearch_pc_test_val = DMZSearch_pc_test.eq(i).find('p').eq(1).text();
				if(DMZSearch_pc_test_val == dmzInput){
					layer.open({
		            	title: '提示信息'
		            	,content: '该IP已被占用'
		        	});
		        	return false;
				}
			}
			dmzFilterAdd_load_pc = layer.load(0, {shade: false});
			dmzFilterAdd_pc();
	    	event.preventDefault();			
		}
	});	
}
//高级功能-防火墙-DMZ过滤-添加-校验-mb
function addDMZBtn_mb(){
	$('.addDMZBtn_mb').off("click").on('click',function () {
		var dmzInput = $('.dmz_add_ip_mb input').val();
		var regIPv4 = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
		if(dmzInput==''){
            layer.open({
                title: '提示信息'
                ,content: '输入内容不能为空,请重新输入'
            });
            return false;
        }
		if(!regIPv4.test(dmzInput)){
			layer.open({
            	title: '提示信息'
            	,content: '请输入正确的IP地址'
        	});
        	return false;				
		} else {
			var DMZSearch_mb_test = $('.DMZSearch_mb');
			var DMZSearch_mb_test_val;
			for (var i = 0; i < DMZSearch_mb_test.length; i++) {
				DMZSearch_mb_test_val = DMZSearch_mb_test.eq(i).find('div').eq(2).find('span').eq(1).text();
				if(DMZSearch_mb_test_val == dmzInput){
					layer.open({
		            	title: '提示信息'
		            	,content: '该IP已被占用'
		        	});
		        	return false;
				}
			}
			dmzFilterAdd_load_mb = layer.load(0, {shade: false});
			dmzFilterAdd_mb();
	    	event.preventDefault();			
		}
	});	
}
//高级功能-防火墙-DMZ过滤-添加-post-pc
function dmzFilterAdd_pc () {
    $.ajax({
        type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
        data: $('#addDmzForm_pc').serialize() + "&but_adddmz=1"
    }).success(function(message) {
    	layer.close(dmzFilterAdd_load_pc);
		layer.open({
	        title: '提示信息'
	        ,icon:6
	        ,content: '添加成功'
	    });
	    $('.dmz_add_ip_pc input').val('');
    }).fail(function(err){
        console.log(err);
    })
}
//高级功能-防火墙-DMZ过滤-添加-post-mb
function dmzFilterAdd_mb () {
    $.ajax({
        type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "firewall")%>',
        data: $('#addDmzForm_mb').serialize() + "&but_adddmz=1"
    }).success(function(message) {
    	layer.close(dmzFilterAdd_load_mb);
			layer.open({
	        title: '提示信息'
	        ,icon:6
	        ,content: '添加成功'
	    });
	    $('.dmz_add_ip_mb input').val('');
    }).fail(function(err){
        console.log(err);
    })
}
//高级功能-4Gconfig
function high_grade_function_4gconfig(){
	var i4g,openf;
	XHR.get('<%=luci.dispatcher.build_url("admin", "advanFunc", "4g")%>',"",function(x, info){
		console.log(info);
		i4g = info.i4g;
		openf = info.openf;
		if(i4g == "1"){
			$('.fourSetInput input').val('未检测到4G卡');
			$('.fourSetSelect').attr('disabled',"disabled");
		}else{
			if(openf == 1){$('.fourSetInput input').val('开启')}else{$('.fourSetInput input').val('关闭')};
			if(i4g == "0"){
				$('.fourSetSelect option').eq(1).attr("selected","selected");
				$('.fourSetSelect').attr('disabled',false);
			}else{
				$('.fourSetSelect option').eq(0).attr("selected","selected")
			};		
		}
	});
	$('.fourSaveApply').off('click').on('click',function () {
		var netKey = $('.fourSetSelect').val();
		console.log(netKey);
		if(netKey == 0){
			$('.fourSetInput input').val('4G正在关闭...');
			index = layer.load(0, {shade: false});
			registPost4GConfig();
		} else if(netKey == 1){
			$('.fourSetInput input').val('4G正在开启...');
			index = layer.load(0, {shade: false});
			registPost4GConfig();
		} else {
			layer.tips('请选择正确的开关方式', '#fourSetSelect',{
			    tips: [2, '#78BA32']
			});
		}
	})
	$('#fourSetSelect').change(function(){
		if(i4g == "1"){
			setTimeout(function(){$('.fourSetInput input').val('开启')},500)
		}else{
			setTimeout(function(){$('.fourSetInput input').val('关闭')},500)
		};
	});
};
//4G请求
function registPost4GConfig () {
	var open4g = $('.fourSetSelect').val();
if(open4g == ''){
		return false;
	}
	var dataForm = 'open4g=' + open4g + '&click=1';
	console.log(dataForm);
    $.ajax({
	    type: "post",
	    url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "4g")%>',
	    data:  dataForm
    }).success(function(message) {
    	console.log(message,open4g);
		layer.close(index);
	}).fail(function(err){
		console.log(err,open4g);
	})
}

//高级功能-网络诊断-init
function high_grade_function_network_init(){
	//Ping请求
	$('.netDiagBtnPing').off('click').on('click',function(){
		update_status('.netDiagInputPing');
	});
	//traceroute请求
	$('.netDiagBtnTrace').off('click').on('click',function(){
		update_status('.netDiagInputTrace');
	});
	//nslookup请求
	$('.netDiagBtnNs').off('click').on('click',function(){
		update_status('.netDiagInputNs');
	});
}
//高级功能-网络诊断-请求 
function update_status(elem){
	var datakey = $(elem).find('input').attr('name');
	var datavalue = $(elem).find('input').val();
	var dataForm = datakey + "=" + datavalue;
	$.ajax({
    	type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "advanFunc","diagnostics")%>',
        data: dataForm
    }).success(function(message) {
        update_status_get(datakey,datavalue);
		$(elem).find('input').val('');
    }).fail(function(err){
    	update_status_get(datakey,datavalue);
    })
}
//高级功能-网络诊断-二次请求 
function update_status_get(name,value){
	var stxhr = new XHR();
	layer.open({
    	type: 1
	    ,title: false //不显示标题栏
    	,closeBtn: false
        ,area: '100%'
    	,shade: 0.4
		,id: '' //设定一个id，防止重复弹出
	    ,btn: ['确定']
	    ,btnAlign: 'c'
    	,moveType: 1 //拖拽模式，0或者1
    	,content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;"><div id="loadings"><img src="<%=resource%>/icons/loading.gif" alt="<%:Loading%>" style="vertical-align:middle" /><%:Waiting for command to complete...%></div></div>'
	});
	var loadings = document.getElementById('loadings');
	stxhr.get('<%=luci.dispatcher.build_url("admin", "advanFunc")%>/diag_' + name + '/' + value, null,function(x){
		if (x.responseText){
			loadings.innerHTML = '<pre>'+x.responseText+'</pre>';
		}else{
			loadings.innerHTML = '<span class="error"><%:Bad address specified!%></span>';
		}
	});
}
//高级功能-RS485-init
function high_grade_function_rs(){
	XHR.get('<%=luci.dispatcher.build_url("admin", "advanFunc", "rs485")%>',"",function(x, info){
		console.log(info);
		var baudRate = info.baudRate,
			parity   = info.parity,
			dataBits = info.dataBits,
			stopBits = info.stopBits;
		for (var i = 0; i < $('.gorgeLineSelect').length; i++) {
			for (var j = 0; j < $('.gorgeLineSelect:eq(' + i + ') option').length; j++) {
				if(i == 0){
					if($('.gorgeLineSelect:eq(' + i + ') option').eq(j).attr("value") == baudRate){
						$('.gorgeLineSelect:eq(' + i + ') option').eq(j).attr("selected","selected");
						break;
					}
				}
				if(i == 1){	
					if($('.gorgeLineSelect:eq(' + i + ') option').eq(j).attr("value") == parity){
						$('.gorgeLineSelect:eq(' + i + ') option').eq(j).attr("selected","selected");
						break;
					}
				}
				if(i == 2){
					if($('.gorgeLineSelect:eq(' + i + ') option').eq(j).attr("value") == dataBits){
						$('.gorgeLineSelect:eq(' + i + ') option').eq(j).attr("selected","selected");
						break;
					}
				}
				if(i == 3){
					if($('.gorgeLineSelect:eq(' + i + ') option').eq(j).attr("value") == stopBits){
						$('.gorgeLineSelect:eq(' + i + ') option').eq(j).attr("selected","selected");
						break;
					}
				}
			}
		}		
	});
	//配置
	$('.glSetting').off('click').on('click',function () {
	    registPostRS485();
	})
};
//RS485请求
function registPostRS485 () {
	var dataForm = $('#RS485Form').serialize()+'&click=1';
    $.ajax({
    	type: "post",
        url: '<%=luci.dispatcher.build_url("admin", "advanFunc", "rs485")%>',
        data: dataForm
    }).success(function(message) {
        console.log(message,dataForm);
		layer.open({
		    title: '提示',
		    icon:6
		    ,content: '配置成功'
		});
    }).fail(function(err){
    	console.log(err,dataForm);
		layer.open({
		    title: '提示',
			icon:6
		    ,content: '配置失败'
		});
    })
}	
// 高级功能-日志
function high_grade_function_log(){
	//生成备份
	$('.creatBackups').off('click').on('click',function(){
		layer.open({
		    title: '提示',
		    content: '确定生成备份？',
		    yes:function(){
		    	var dataForm;
		    	$.ajax({
			    	type: "post",
			        url: '<%=luci.dispatcher.build_url("admin", "", "")%>',
			        data: dataForm
			    }).success(function(message) {
			        console.log(dataForm);
					layer.open({
					    title: '提示',
					    icon:6
					    ,content: '生成备份成功'
					});
			    }).fail(function(err){
			    	console.log(err,dataForm);
					layer.open({
					    title: '提示',
						icon:5
					    ,content: '生成备份失败'
					});
			    })
		    }
		});
	})
}
// 高级功能-Zigbee参数-tab
function high_grade_function_zigbee(){
	XHR.get('<%=luci.dispatcher.build_url("admin", "status", "info")%>',{status:3},function(x, info){
		console.log(info);
		//更新center数据
		var center = info.center;
		$('.ZigbeeSearch p').eq(0).html(center.macaddr);
		$('.ZigbeeSearch p').eq(1).html(center.channel);
		$('.ZigbeeSearch p').eq(2).html(center.panId);
		$('.ZigbeeSearch').css({"color":"black"});
		if(info.hasOwnProperty("apTable")){
			var apTable = info.apTable;
			if(apTable.length < 1){
				return false;
			}else{
				var zigbee_init_html = "";
				for (var i = 0; i < apTable.length; i++) {
					if(apTable[i].status == "1"){
						zigbee_init_html += '<div class="flex_h align_center list_item_uniform"><p>'
											+ apTable[i].macaddr 
											+'</p><p>'
											+ apTable[i].channel 
											+'</p><p>'
											+ apTable[i].panid 
											+'</p></div>';
					}
				}
				//更新apTable数据
				if(zigbee_init_html != ""){
					$('.ZigbeeSearch').nextAll().remove();
					$('.ZigbeeList').append(zigbee_init_html);
				}					
			}
		}
	});
}	
/**
 * 4. System setup
 * */
//系统设置--密码设置
function set_password_init(){
	$('.sureChangePWD').off('click').on('click',function(){
		set_password_request();
	})
}
//系统设置--密码设置-请求
function set_password_request(){
	var pwdConfigSelect = $('.pwdConfigSelect').val();
	var password = $('.password').find('input').val().trim().replace(/\s/g,"");
	var passwordAgain = $('.passwordAgain').find('input').val().trim().replace(/\s/g,"");
	if(pwdConfigSelect == ''){
		layer.open({
		    title: '提示',
		    content: '请选择需要修改的用户名'
		});
	}else if((password == '') || (passwordAgain == '')){
		layer.open({
		    title: '提示',
		    content: '请输入需要修改的密码'
		});
		$('.password').find('input').val('');
		$('.passwordAgain').find('input').val('');
	}else if(password != passwordAgain){
		layer.open({
		    title: '提示',
		    content: '请保证两次密码一致'
		});
		$('.password').find('input').val('');
		$('.passwordAgain').find('input').val('');
	}else if((password.length > 16) || (password.length < 5)){
		layer.open({
		    title: '提示',
		    content: '输入密码长度在5-16位之间'
		});
	}else{
		var dataForm = 'user=' + pwdConfigSelect + '&newPWD=' + password + '&newPWDAgain=' + passwordAgain + '&click=1';
		$.ajax({
	    	type: "post",
	        url: '<%=luci.dispatcher.build_url("admin", "system", "password")%>',
	        data: dataForm
	    }).success(function(message) {
	        console.log(message,dataForm);
			layer.open({
			    title: '提示',
			    icon:6
			    ,content: '配置密码成功'
			});
			setTimeout(function(){
				localStorage.clear();
    			window.location.href = '<%=luci.dispatcher.build_url("admin", "logout")%>';
			},2000)
	    }).fail(function(err){
	    	console.log(err,dataForm);
			layer.open({
			    title: '提示',
				icon:6
			    ,content: '配置密码失败'
			});
			$('.password').find('input').val('');
			$('.passwordAgain').find('input').val('');
	    })
	}
}
//系统设置--恢复出厂设置
function restore_init(){
	$('.recoverSet').off('click').on('click',function(){
		layer.confirm('设备将恢复出厂设置，是否确认执行此操作？',{btn: ['确定','取消']},function(index){
			var upload = layer.load(0, {shade: false});
	    	var dataForm = 'click=1';
	    	$.ajax({
		    	type: "post",
		        url: '<%=luci.dispatcher.build_url("admin", "system", "reset")%>',
		        data: dataForm
		    }).success(function(data) {
		    	layer.closeAll('dialog');
		    	layer.close(upload);
		    	console.log(data,dataForm);
		        var isRefresh = '<img src="<%=resource%>/icons/loading.gif" alt="<%:Loading%>" style="vertical-align:middle" /> 正在重启并恢复出厂设置';
				var model = '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;"><div id="loadings"><div>'+ isRefresh +'</div></div></div>';
				layer.open({
				    type: 1
				    ,title: false //不显示标题栏
				    ,closeBtn: false
	        		,area: '50%'
					,shade: 0.4
					,id: '' //设定一个id，防止重复弹出
					,btn: ['确定']
					,btnAlign: 'c'
					,moveType: 1 //拖拽模式，0或者1
					,content: model 
				});
		    }).fail(function(err){
		    	layer.closeAll('dialog');
		    	layer.close(upload);
		    	console.log(err,dataForm);
				layer.open({
				    title: '提示',
					icon:5
				    ,content: '恢复出厂设置失败'
				});
		    })
		})
	})
}
//系统设置-备份升级
function upgrade_init(){
	$('.upPackage').off('click').on('click',function(){
   		var form = $('<form>');  
		form.attr('style','display:none');  
		form.attr('target','');
		form.attr('enctype','multipart/form-data');
		form.attr('method','post');  
		form.attr('action','<%=luci.dispatcher.build_url("admin", "system", "flashops")%>');
		var Name = $('<input>');
		Name.attr('type','text');
		Name.attr('name','backup');
		Name.attr('value',1);
		$('body').append(form);
		form.append(Name);
		form.submit();  
		form.remove();
	})
	//上传备份
	$('.upBackups').off('click').on('click',function(){
		var fileObj = $(".update").val();
        if (fileObj == "") {
            layer.open({
			    title: '提示',
				icon:5
			    ,content: '请先选择文件'
			});
            return false;
        }
		layer.confirm('设备将恢复配置，是否确认执行此操作？',{btn: ['确定','取消']},function(index){
			var upload = layer.load(0, {shade: false});
			var inputRestore = $('<input>');
			inputRestore.attr('style','display:none');  
			inputRestore.attr('type','text');
			inputRestore.attr('class','inputRestore');
			inputRestore.attr('name','restore');
			inputRestore.attr('value',1);
			$('#updataPack').append(inputRestore);
			$('#updataPack').ajaxSubmit({
        		type: 'post', 
	            url: '<%=luci.dispatcher.build_url("admin", "system", "flashops")%>',
				success: function(data) {                		
        	        console.log(data);
					var isRefresh = '<img src="<%=resource%>/icons/loading.gif" alt="<%:Loading%>" style="vertical-align:middle" /> 正在重启并恢复备份文件';
					var model = '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;"><div id="loadings"><div>'+ isRefresh +'</div></div></div>';
					layer.open({
					    type: 1
					    ,title: false
					    ,closeBtn: false
		        		,area: '50%'
						,shade: 0.4
						,id: ''
    					,btn: ['确定']
    					,btnAlign: 'c'
						,moveType: 1
						,content: model 
					});
					layer.close(upload);
					$('.inputRestore').remove();
		        },
				error:function(){
					layer.close(upload);
					layer.open({
			   			title: '提示',
						icon:5
			    		,content: '升级失败请联系管理员!'
					});
				}
	        });
			layer.close(index);
			return false;
		});
	})
	//在线升级固件
	$('.onlineUpdata').off('click').on('click',function(){
		var fileObj = $(".upLocal").val();
		if (fileObj == "") {
            layer.open({
			    title: '提示',
				icon:5
			    ,content: '请先选择文件'
			});
            return false;
        }
		var dataForm = new FormData();
		dataForm.append('file', $("#image")[0].files[0]);
		dataForm.append("keep",1);
		layer.confirm('设备将重启并刷新固件，是否确认执行此操作？',{btn: ['确定','取消']},function(index){
			var upload = layer.load(0, {shade: false});
			$('#updateLoal').ajaxSubmit({
        		type: 'post', // 提交方式 get/post
	            url: '<%=luci.dispatcher.build_url("admin", "system", "flashops")%>',
				success: function(data) {                		
        	        console.log(data);
					var isRefresh = '<img src="<%=resource%>/icons/loading.gif" alt="<%:Loading%>" style="vertical-align:middle" /> 刷写中...'
					var isCheck = (data.checksum) == "true" ? "校验成功!" : "校验失败!"
					var isSave = (data.keep) == true ? "配置文件将被保存!" : "配置文件将不被保存!"
					var canUse = "固件文件大小:" + parseFloat((data.size)/1024/1024).toFixed(3) + 'MB';
					var model = '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;"><div id="loadings"><div>'+ isRefresh +'</div><div>'+isCheck+'</div><div>'+ isSave +'</div><div>'+ canUse +'</div></div></div>';
					layer.open({
					    type: 1
					    ,title: false //不显示标题栏
					    ,closeBtn: false
		        		,area: '50%'
						,shade: 0.4
						,id: '' //设定一个id，防止重复弹出
    					,btn: ['确定']
    					,btnAlign: 'c'
						,moveType: 1 //拖拽模式，0或者1
						,content: model 
					});
					layer.close(upload);
		        },
				error:function(){
					layer.close(upload);
					layer.open({
			   			title: '提示',
						icon:5
			    		,content: '升级失败请联系管理员!'
					});
				}
	        });
			layer.close(index);
			return false;
		});
	})
}
//系统设置--周期任务
function cycle_tasks_init(){
	XHR.get('<%=luci.dispatcher.build_url("admin", "system", "periodFunc")%>',null,function(x, info){
		console.log(info);
		if(info.disabled == '1'){
			$('.switch_1[name=perdis]').attr('checked',false);
		}else{
			$('.switch_1[name=perdis]').attr('checked',true);
		}
		$('.perdDIY').find('input').val(info.perd);
	});
	$('.perdSure').off('click').on('click',function(){
		cycle_tasks_request();
	})
}
//周期任务--请求
function cycle_tasks_request(){
	var perd = $('.perdDIY').find('input').val();
	var perdis = $('.switch_1[name=perdis]').prop('checked') ? 0: 1;
	if(perd == ''){
		layer.open({
			title: '提示',
			content: '重启周期不能为空'
		})
	}else if(!((perd.indexOf("d") != -1) || (perd.indexOf("h") != -1) || (perd.indexOf("m") != -1))){
		layer.open({
			title: '提示',
			content: '请输入正确的时间单位'
		})
	}else{
		var dataForm = 'perdis=' + perdis + '&perd=' +perd +'&click=1';
	    $.ajax({
	    	type: "post",
	        url: '<%=luci.dispatcher.build_url("admin", "system", "periodFunc")%>',
	        data: dataForm
	    }).success(function(message) {
	        console.log(message,dataForm);
			layer.open({
			    title: '提示',
			    icon:6
			    ,content: '配置成功'
			});
	    }).fail(function(err){
	    	console.log(err,dataForm);
			layer.open({
			    title: '提示',
				icon:5
			    ,content: '配置失败'
			});
	    })
	}
}
//系统设置--重启
function restart_init(){
	$('.reStart').off('click').on('click',function(){
		layer.confirm('设备将重启，是否确认执行此操作？'
		,{btn: ['确定','取消']}
		,function(index){
			var upload = layer.load(0, {shade: false});
	    	var dataForm = 'click=1';
	    	$.ajax({
		    	type: "post",
		        url: '<%=luci.dispatcher.build_url("admin", "system", "reboot")%>',
		        data: dataForm
		    }).success(function(data) {
		    	layer.close(upload);
		    	layer.closeAll('dialog');
		    	console.log(data,dataForm);
		        var isRefresh = '<img src="<%=resource%>/icons/loading.gif" alt="<%:Loading%>" style="vertical-align:middle" /> 正在重启';
				var model = '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;"><div id="loadings"><div>'+ isRefresh +'</div></div></div>';
				layer.open({
				    type: 1
				    ,title: false
				    ,closeBtn: false
	        		,area: '50%'
					,shade: 0.4
					,id: ''
					,btn: ['确定']
					,btnAlign: 'c'
					,moveType: 1
					,content: model 
				});
		    }).fail(function(err){
		    	layer.close(upload);
		    	layer.closeAll('dialog');
				layer.open({
				    title: '提示',
					icon:5
				    ,content: '重启失败'
				});
		    })
		});
	})
}
